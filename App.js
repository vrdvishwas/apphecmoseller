/* eslint-disable global-require */
import React, { Component } from "react";

import OneSignal from 'react-native-onesignal';
import { Text, AsyncStorage } from 'react-native';
import { Input } from 'native-base';
// eslint-disable-next-line import/no-extraneous-dependencies
import { Provider } from 'react-redux';

import Router from './Router';
import configureStore from './store/store';

const store = configureStore();

export default class App extends Component {

  constructor(props) {
    super(props);
    Input.defaultProps.allowFontScaling = false;
        Text.defaultProps = { ...(Text.defaultProps || {}), allowFontScaling: false };
    this.state = {
      isReady: false,
    };

    OneSignal.init("5123111b-d184-4730-91e0-ba23745ef0b6");
      // this.oneSignalInAppMessagingExamples();
      OneSignal.setLogLevel(6, 0);
  }

  async componentDidMount() {
    this.setState({ isReady: true });
    var providedConsent = await OneSignal.userProvidedPrivacyConsent();

    // this.setState({privacyButtonTitle : `Privacy Consent: ${providedConsent ? "Granted" : "Not Granted"}`, privacyGranted : providedConsent});
  
    OneSignal.setLocationShared(true);
   
    OneSignal.inFocusDisplaying(2);

    this.onReceived = this.onReceived.bind(this);
    this.onOpened = this.onOpened.bind(this);
    this.onIds = this.onIds.bind(this);
    // this.onEmailRegistrationChange = this.onEmailRegistrationChange.bind(this);
    this.onInAppMessageClicked = this.onInAppMessageClicked.bind(this);
  
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    // OneSignal.addEventListener('emailSubscription', this.onEmailRegistrationChange);
    OneSignal.addEventListener('inAppMessageClicked', this.onInAppMessageClicked);
  }


  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
    // OneSignal.removeEventListener('emailSubscription', this.onEmailRegistrationChange);
    OneSignal.removeEventListener('inAppMessageClicked', this.onInAppMessageClicked);
}

onReceived(notification) {
  console.log("Notification received: ", notification);
  // this.setState({jsonDebugText : "RECEIVED: \n" + JSON.stringify(notification, null, 2)})
}

onOpened(openResult) {
console.log('Message: ', openResult.notification.payload.body);
console.log('Data: ', openResult.notification.payload.additionalData);
console.log('isActive: ', openResult.notification.isAppInFocus);
console.log('openResult: ', openResult);

// this.setState({jsonDebugText : "OPENED: \n" + JSON.stringify(openResult.notification, null, 2)})
}

async onIds(device) {
  console.log('Device info: ', device);
  await AsyncStorage.setItem('nfid', device.userId);
}
  
  onInAppMessageClicked(actionResult) {
  console.log('actionResult: ', actionResult);
  // this.setState({jsonDebugText : "CLICKED: \n" + JSON.stringify(actionResult, null, 2)})
  }
 
  render() {
  return (
    <Provider store={store}>
    <Router />
  </Provider>
  );
}
}
