import React, { Component } from 'react';
import {
    Container,
    Content,
    Button,
    Text,
    Form,
    Item,
    Input
} from 'native-base';
import {connect} from 'react-redux';
import { View, Image, AsyncStorage, ActivityIndicator, TouchableOpacity, Alert, StyleSheet } from 'react-native';
// import { LoginButton, LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import styles from '../styles';
import {DataCall} from '../utils/DataCall';
import axios from 'axios';
import { userData, tokenValue } from '../store/actions/TokenAction';
// import {
//   GoogleSignin,
//   statusCodes,
//   GoogleSigninButton
// } from '@react-native-community/google-signin';

// const styless = StyleSheet.create({
//   defaultButtonStyle: {
//     height: 50,
//     width: "80%",
//     backgroundColor: "#48c7e8"
//   },
// });

// LoginButton.defaultProps = {
//   style: styless.defaultButtonStyle,
// };

class LoginScreen extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            passwordValidate: true,
            emailValidate: true,
            error:'',
            status:'',
            showLoader: false
            };
            this.checkUser = this.checkUser.bind(this);
    }

    // async componentDidMount() {
    //   this._configureGoogleSignIn();
    //   await this._getCurrentUser();
    // }
  
    // _configureGoogleSignIn() {
    //   GoogleSignin.configure({
    //     webClientId: '800265347004-77ol4plaj5hbs0upa7eoj8br4vtsekd6.apps.googleusercontent.com',
    //     offlineAccess: true,
    //   });
    // }
  
    // async _getCurrentUser() {
    //   try {
    //     const userInfo = await GoogleSignin.signInSilently();
    //     this.setState({ userInfo, error: null });
    //   } catch (error) {
    //     const errorMessage =
    //       error.code === statusCodes.SIGN_IN_REQUIRED ? 'Please sign in :)' : error.message;
    //     this.setState({
    //       error: new Error(errorMessage),
    //     });
    //   }
    // }

    validate(text, type) {
       const emails = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        if (type == 'email') {
            if (emails.test(text)) {
                this.setState({ emailValidate: true });
            } else {
                this.setState({ emailValidate: false });
        }
        this.setState({ email:text });
     }
        else if(type == 'password') {
        this.setState({ password: text });
        }
        }

        // async initUser(token) {
        //     await fetch('https://graph.facebook.com/v2.5/me?fields=email,name,friends&access_token=' + token)
        //     .then((response) =>{
        //         console.log(response.json());
        //     }).catch(() => {
        //       reject('ERROR GETTING DATA FROM FACEBOOK')
        //     })
        //   }


 async checkUser() { 
    
    this.setState({ showLoader: true });
    try {
     var loginRequest= await axios.post('https://www.hecmo.com/rest/V1/seller/login', {
        email: this.state.email,
        password: this.state.password,
      });
     var loginStatus = await loginRequest.status;
    } 
    catch(error) {
        this.setState({ showLoader: false });
    }

      if(loginStatus == 200) {
     const loginResponse = loginRequest.data;
        // console.log(loginResponse);
        console.log(loginResponse);
          await AsyncStorage.setItem('token', loginResponse);  
          const profileCall= await DataCall.profileData(loginResponse);
        //   const homeCall= await DataCall.homeData(loginResponse);
          await this.props.updateToken(loginResponse);
          await this.props.updateUserData(profileCall);
          await AsyncStorage.getItem('nfid').then((nfid) => {
            DataCall.OnesignalUpdateCustomUserID(nfid,profileCall.hash_hmac);
          });
        this.props.navigation.navigate('Home');
    }
    }

    // _responseInfoCallback = (error, result) => {
    //     if (error) {
    //       alert('Error fetching data: ' + error.toString());
    //     } else {
    //         console.log("result",result);
    //       console.log('Result Name: ' + result.name);
    //       console.log('Result Email: ' + result.email);
    //     }
    //   }

    //   signInFacebook(){
    //     LoginManager.logInWithPermissions(["email","name"]).then(
    //       function(result) {
    //         if (result.isCancelled) {
    //           console.log("Login cancelled");
    //         } else {
    //           console.log(
    //             "Login success with permissions: " +
    //               result.grantedPermissions.toString()
    //           );
    //         }
    //       },
    //       function(error) {
    //         console.log("Login fail with error: " + error);
    //       }
    //     );
    //   }
    
    //   _signIn = async () => {
    //     try {
    //       await GoogleSignin.hasPlayServices();
    //       const userInfo = await GoogleSignin.signIn();
    //       await GoogleSignin.revokeAccess();
    //     console.log("google user userInfo",userInfo);
    //       // this.setState({ userInfo, error: null });
    //     } catch (error) {
    //       if (error.code === statusCodes.SIGN_IN_CANCELLED) {
    //         // sign in was cancelled
    //         Alert.alert('cancelled');
    //       } else if (error.code === statusCodes.IN_PROGRESS) {
    //         // operation in progress already
    //         Alert.alert('in progress');
    //       } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
    //         Alert.alert('play services not available or outdated');
    //       } else {
    //         Alert.alert('Something went wrong', error.toString());
    //       }
    //     }
    //   };

    //   _signOut = async () => {
    //     try {
    //       await GoogleSignin.revokeAccess();
    //       await GoogleSignin.signOut();
    
    //       this.setState({ userInfo: null, error: null });
    //     } catch (error) {
    //       this.setState({
    //         error,
    //       });
    //     }
    //   };

     render() {
                return (
            <Container style={{ marginTop: 20 }}>
                <Image style={styles.loginimages} source={require('../assets/logo.png')} />
                <Content>
                    <Form>
                        <Item floatingLabel>
                            <Input
                                placeholder="Email ID"
                                style={[!this.state.emailValidate
                                    ? styles.loginerror
                                    : null]}
                                onChangeText={(text) => this.validate(text, 'email')}

                            />
                        </Item>
                        <Item floatingLabel>
                            <Input placeholder="Password"  
                            onChangeText={(text) => this.validate(text, 'password')}
                            secureTextEntry= {true}
                            />
                        </Item>
                    </Form>

                    <Button style={styles.loginbtn}>
                        <Text onPress={() => this.checkUser()} style={styles.logintxt}>{this.state.showLoader?'PLEASE WAIT..':'LOGIN'}</Text>
                    </Button>
{/* <View>
  <View>
                    <LoginButton
          // Below "publishPermissions" is replaced by "permissions" according to github/fbsdk/issues
          style={{  height: 50, width: "80%", alignSelf: "center", marginVertical: 5, justifyContent:"center" }}
          permissions={["email"]}
          onLoginFinished={
            (error, result) => {
              if (error) {
                alert("Login failed with error: " + error.message);
              } else if (result.isCancelled) {
                alert("Login was cancelled");
              } else {
                AccessToken.getCurrentAccessToken().then(
                    (data) => {
                        const infoRequest = new GraphRequest(
                            '/me?fields=name,email,picture.type(large)',
                            null,
                            this._responseInfoCallback
                          );
                          new GraphRequestManager().addRequest(infoRequest).start();    
                    }
                  )
              }
            }
          }
          onLogoutFinished={() => alert("Logged Out")}/>

  </View> */}


  {/* <View style={{ width: "90%" }}>
          <GoogleSigninButton
          style={{ height: 50, width: "89%", backgroundColor: "#48c7e8", marginVertical: 5, marginLeft: 35, alignSelf: "center" }}
          size={GoogleSigninButton.Size.Standard}
          color={GoogleSigninButton.Color.Auto}
          onPress={this._signIn}
        />
        </View>
        </View> */}
                {/* <Button onPress={this._signOut} style={styles.loginbtn}><Text>Log out</Text></Button> */}
                    {this.state.showLoader ?
                        <ActivityIndicator animating = {true} size="large" /> : null}
                    
                    <Text
                        style={styles.loginforgot}
                        onPress={() => this.props.navigation.navigate('ForgotPassword')}
                    >Forgot Password</Text>
                    <Text
                        style={styles.logindont}
                    >Don't have Account?  
                    <Text
                        style={{
                        color: '#48c0e8',
                        marginLeft: 5
                    }}
                    onPress={() => this.props.navigation.navigate('Register')}
                    > Signup</Text> </Text>
                    
                </Content>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
	updateToken: (token) => {
		dispatch(tokenValue(token));
	},
	updateUserData: (data) => {
		dispatch(userData(data));
	}
}); 

export default connect(null, mapDispatchToProps)(LoginScreen);