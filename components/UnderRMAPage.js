import React from 'react';
import {
  Platform,
  View,
  Text,
} from 'react-native';
import {Container, Card, Icon} from 'native-base';

import styles from '../styles';

const isIOS = Platform.OS === 'android';

export class UnderRMAPage extends React.Component {
  shouldComponentUpdate(newProps) {
    return this.props.product !== newProps.product;
  }

  componentWillUpdate() {
    //On iOS while recycling till the new image is loaded the old one remains visible. This forcefully hides the old image.
    //It is then made visible onLoad
    if (isIOS && this.imageRef) {
      this.imageRef.setNativeProps({
        opacity: 0,
      });
    }
  }

  handleOnLoad = () => {
    if (isIOS && this.imageRef) {
      this.imageRef.setNativeProps({
        opacity: 1,
      });
    }
  };

  render() {
    let item = this.props.product;
    let token = this.props.nav.token;
    return (
      <View style={{width: '100%' }}>
      <Card style={{ backgroundColor: "#f4f4f4", padding: 10, height: "auto" }}>

<View style={{ flexDirection: "row" }}>
  
<Text style={{ fontSize: 15, fontWeight: "bold", marginHorizontal: 10 }}>Order ID</Text>
<Text style={{ position: "absolute", marginLeft: "40%" }}>:</Text>
<Text style={{ position: "absolute", marginLeft: "45%" }}>{item.order_id}</Text>
</View>
  
<View style={{ flexDirection: "row" }}>
  
<Text style={{ fontSize: 15, fontWeight: "bold", marginHorizontal: 10 }}>Customer Name</Text>
<Text style={{ position: "absolute", marginLeft: "40%" }}>:</Text>
<Text style={{ position: "absolute", marginLeft: "45%" }}>{item.customer_name}</Text>
</View>
  
<View style={{ flexDirection: "row" }}>
  
<Text style={{ fontSize: 15, fontWeight: "bold", marginHorizontal: 10 }}>Product Name</Text>
<Text style={{ position: "absolute", marginLeft: "40%" }}>:</Text>
<Text style={{ position: "absolute", marginLeft: "45%" }} numberOfLines={1}>{item.product_name}</Text>
</View>
  
<View style={{ flexDirection: "row" }}>
  
<Text style={{ fontSize: 15, fontWeight: "bold", marginHorizontal: 10 }}>Reason</Text>
<Text style={{ position: "absolute", marginLeft: "40%" }}>:</Text>
<Text style={{ position: "absolute", marginLeft: "45%" }}>{item.reason}</Text>
</View>

<View style={{ flexDirection: "row" }}>
  
<Text style={{ fontSize: 15, fontWeight: "bold", marginHorizontal: 10 }}>Date</Text>
<Text style={{ position: "absolute", marginLeft: "40%" }}>:</Text>
<Text style={{ position: "absolute", marginLeft: "45%" }}>{item.date}</Text>
</View>

<View style={{ flexDirection: "row" }}>
  
<Text style={{ fontSize: 15, fontWeight: "bold", marginHorizontal: 10 }}>Return/Cancel St.</Text>
<Text style={{ position: "absolute", marginLeft: "40%" }}>:</Text>
<Text style={{ position: "absolute", marginLeft: "45%" }}>{item.rma_status}</Text>
</View>

<View style={{ flexDirection: "row" }}>
  
<Text style={{ fontSize: 15, fontWeight: "bold", marginHorizontal: 10 }}>My Status</Text>
<Text style={{ position: "absolute", marginLeft: "40%" }}>:</Text>
<Text style={{ position: "absolute", marginLeft: "45%" }}>{item.my_status}</Text>
</View>

<View style={{ flexDirection: "row" }}>
  
<Text style={{ fontSize: 15, fontWeight: "bold", marginHorizontal: 10 }}>Customer Status</Text>
<Text style={{ position: "absolute", marginLeft: "40%" }}>:</Text>
<Text style={{ position: "absolute", marginLeft: "45%" }}>{item.customer_status}</Text>
</View>

<View style={{ flexDirection: "row" }}>
  
<Text style={{ fontSize: 15, fontWeight: "bold", marginHorizontal: 10 }}>Consignment No.</Text>
<Text style={{ position: "absolute", marginLeft: "40%" }}>:</Text>
<Text style={{ position: "absolute", marginLeft: "45%" }}>{item.consignment_number}</Text>
</View>

<View style={{ flexDirection: "row" }}>
  
<Text style={{ fontSize: 15, fontWeight: "bold", marginHorizontal: 10 }}>Quantity</Text>
<Text style={{ position: "absolute", marginLeft: "40%" }}>:</Text>
<Text style={{ position: "absolute", marginLeft: "45%" }}>{item.quantity}</Text>
</View>

      </Card>
      </View>
    );
  }
}
