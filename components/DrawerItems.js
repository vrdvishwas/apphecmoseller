/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import { View, Image, Text, AsyncStorage} from 'react-native';
import { Container, Content, List, ListItem } from 'native-base';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import styles from '../styles';
import SvgUri from 'react-native-svg-uri';
import { userData, tokenValue } from '../store/actions/TokenAction';

const drawerImage = require('../assets/logo.png');

const brands = [
	{
		name: 'My Dashboard',
		route: 'Dashboard',
		image: 'https://www.hecmo.com/media/mobile/seller/My_Dashboard.svg'
	},
	{
		name: 'Seller Profile',
		route: 'Profile',
		image: 'https://www.hecmo.com/media/mobile/seller/Seller_Profile.svg'
	},
	{
		name: 'My Order History',
		route: 'OderHistory',
		image: 'https://www.hecmo.com/media/mobile/seller/My_Order_History.svg'
	},
	{
		name: 'My Transaction List',
		route: 'Transaction',
		image: 'https://www.hecmo.com/media/mobile/seller/My_Transaction_List.svg'
	},
	{
		name: 'My Product List',
		route: 'MyProductList',
		image: 'https://www.hecmo.com/media/mobile/seller/My_Products_List.svg'
	},
	{
		name: 'Shipping',
		route: 'Shipping',
		image: 'https://www.hecmo.com/media/mobile/seller/Shipping.svg'
	},
	{
		name: 'My Details',
		route: 'MyDetails',
		image: 'https://www.hecmo.com/media/mobile/seller/My_Details.svg'
	}
];

class DrawerItems extends Component {
	constructor(props) {
		super(props);
		this.state = {
			shadowOffsetWidth: 1,
			shadowRadius: 4,
			isLogin: true
		};
	}

	async logoutFunction() {
		await AsyncStorage.removeItem('token');
		await this.props.updateToken(null);
		await this.props.updateUserData({});
		this.props.navigation.navigate('BeforeLogin');
	}
	
	render() {
		return (
			<Container>
				<Content bounces={false} style={{ flex: 1, backgroundColor: '#fff', top: 12 }}>
					<View style={{ minHeight: 80, justifyContent: "center", marginHorizontal: 20 }}>
					<Image source={drawerImage} style={{ height: 50, width: 120 }} />
					</View>
					<Text style={{ fontSize: 18, fontWeight: '400', marginLeft: 20 }}>
						Hi, {this.props.user.shoptitle ? this.props.user.shoptitle : 'Anonymous'}
					</Text>
					<List
						dataArray={brands}
						renderRow={(data) => (
							<ListItem button noBorder onPress={() => {data.route=='Dashboard' ? this.props.navigation.closeDrawer():this.props.navigation.navigate(data.route)}}>
								<SvgUri
								style={{ marginRight: 6, marginLeft: 10 }}
									 width="26"
									 height="26"
									source={{ uri : data.image }}
								/>

								<Text style={{ fontSize: 15, marginLeft: 6 }}>{data.name}</Text>
							</ListItem>
						)}
					/>
					<View style={{ flexDirection:'row', marginTop: 15, marginLeft: 5 }}>
						<SvgUri
							style={{ marginRight: 6, marginLeft: 30 }}
							width="26"
							height="26"
							source={{ uri: 'https://www.hecmo.com/media/mobile/seller/logout.svg' }}
						/>
						<Text
							style={{ fontSize: 15, marginLeft: 6 }}
							onPress={this.state.isLogin == true ? this.logoutFunction.bind(this) : null}
						>
							Logout
						</Text>
					</View>
				</Content>
			</Container>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		token: state.HAReduce.token,
		user: state.HAReduce.user
		};
};

const mapDispatchToProps = (dispatch) => ({
	updateToken: (token) => {
		dispatch(tokenValue(token));
	},
	updateUserData: (data) => {
		dispatch(userData(data));
	}
});

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(DrawerItems));
