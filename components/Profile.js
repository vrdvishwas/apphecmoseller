import React, {Component} from 'react';
import {
  Image,
  View,
  KeyboardAvoidingView,
  Platform,
  StatusBar,
  Button,
  ActivityIndicator,
  Modal, 
  Linking,
  TouchableOpacity
} from 'react-native';
import {
  Container,
  Header,
  Card,
  Icon,
  Left,
  Body,
  Title,
  Right,
  Text,
  Form,
  Input,
  Item,
  Label,
  Content,
} from 'native-base';
import SvgUri from 'react-native-svg-uri';
import ImagePicker from 'react-native-image-picker';
import {Dropdown} from 'react-native-material-dropdown';
import {connect} from 'react-redux';
import {DataCall} from '../utils/DataCall';
import {userData, tokenValue} from '../store/actions/TokenAction';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import styles from '../styles';

const options = {
  title: 'Select a Photo',
  takePhotoButtonTitle: 'Take a Photo',
  chooseFromLibraryButtonTitle: 'choose from gallery',
  quality: 1,
};


class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTag: 'body',
      selectedStyles: [],
      isVerify: 0,
      shoptitle: '',
      contactnumber: '',
      complocality: '',
      facebookid: '',
      bannerlink: '',
      meta_keyword: '',
      meta_description: '',
      seller_pan: '',
      seller_pan_name: '',
      seller_annual_turnover: '',
      seller_pan_business: '',
      seller_gstin: '',
      mode: false,
      TextInputDisableStatus: true,
      bannerpic: null,
      logopic: null,
      seller_cancel_cheque: null,
      address_proof: null,
      signature: null,
      isUploading: false,
      loader: true,
      showPopup: false
    };
    this.editor = null;
    this.selectImage = this.selectImage.bind(this);
  }
  async updatingProfile() {
    let profileData = new FormData();
    if(this.state.shoptitle!="") {
      profileData.append('shoptitle', this.state.shoptitle);
    }
    if(this.state.contactnumber!="") {
    profileData.append('contactnumber', this.state.contactnumber);
    }
    if(this.state.complocality!="") {
      profileData.append('complocality', this.state.complocality);
    }
    if(this.state.facebookid!="") {
      profileData.append('facebookid', this.state.facebookid);
    }
    if(this.state.bannerlink!="") {
      profileData.append('bannerlink', this.state.bannerlink);
    }
    if(this.state.meta_keyword!="") {
      profileData.append('meta_keyword', this.state.meta_keyword);
    }
    if(this.state.meta_description!="") {
      profileData.append('meta_description', this.state.meta_description);
    }
    if(this.state.seller_pan!="") {
      profileData.append('seller_pan', this.state.seller_pan);
    }
    if(this.state.seller_pan_name!="") {
      profileData.append('seller_pan_name', this.state.seller_pan_name);
    }
    if(this.state.seller_annual_turnover!="") {
      profileData.append('seller_annual_turnover',this.state.seller_annual_turnover);
    }
    if(this.state.seller_pan_business!="") {
      profileData.append('seller_pan_business', this.state.seller_pan_business);
    }
    if(this.state.seller_gstin!="") {
      profileData.append('seller_gstin', this.state.seller_gstin);
    }
    if(this.state.address_proof!=null) {
    profileData.append('address_proof', {
      type: `image/jpeg`,
      uri: this.state.address_proof,
      name: `photo.jpeg`,
    });
  }
  if(this.state.seller_cancel_cheque!=null) {
    profileData.append('seller_cancel_cheque', {
      type: `image/jpeg`,
      uri: this.state.seller_cancel_cheque,
      name: `photo.jpeg`,
    });
  }
  if(this.state.signature!=null) {
    profileData.append('signature', {
      type: `image/jpeg`,
      uri: this.state.signature,
      name: `photo.jpeg`,
    });
  }
  if(this.state.bannerpic!=null) {
    profileData.append('bannerpic', {
      type: `image/jpeg`,
      uri: this.state.bannerpic,
      name: `photo.jpeg`,
    });
  }
  if(this.state.logopic!=null) {
    profileData.append('logopic', {
      type: `image/jpeg`,
      uri: this.state.logopic,
      name: `photo.jpeg`,
    });
  }
    await DataCall.updateProfile(profileData, this.props.token);
    const rs = await DataCall.profileData(this.props.token);
    await this.props.updateUserData(rs);

    this.props.navigation.goBack();
  }

  async updateState(text, type) {
    if (type == 'shoptitle') {
      this.setState({shoptitle: text});
    } 
    else {
      this.props.user.shoptitle;
    }
    if (type == 'contactnumber') {
      this.setState({contactnumber: text});
    }
    if (type == 'contactnumber') {
      this.setState({contactnumber: text});
    }
    if (type == 'complocality') {
      this.setState({complocality: text});
    }
    if (type == 'facebookid') {
      this.setState({facebookid: text});
    }
    if (type == 'bannerlink') {
      this.setState({bannerlink: text});
    }
    if (type == 'meta_keyword') {
      this.setState({meta_keyword: text});
    }
    if (type == 'meta_description') {
      this.setState({meta_description: text});
    }
    if (type == 'seller_pan') {
      this.setState({seller_pan: text});
    }
    if (type == 'seller_pan_name') {
      this.setState({seller_pan_name: text});
    }
    if (type == 'seller_annual_turnover') {
      this.setState({seller_annual_turnover: text});
    }
    if (type == 'seller_pan_business') {
      this.setState({seller_pan_business: text});
    }
    if (type == 'seller_gstin') {
      this.setState({seller_gstin: text});
    }
    await DataCall.profileData(this.props.token);
  }

  edit(mode) {
    setTimeout(() => {
      this.setState({
        loader: false,
      });
    }, 0);

    if (mode == true) {
      this.setState({isVerify: 1});
      this.setState({TextInputDisableStatus: false});
    } else {
      this.setState({isVerify: 0});
    }
  }
  onStyleKeyPress = toolType => {
    this.editor.applyToolbar(toolType);
  };
  onSelectedTagChanged = tag => {
    this.setState({
      selectedTag: tag,
    });
  };
  onSelectedStyleChanged = styles => {
    this.setState({
      selectedStyles: styles,
    });
  };
  onValueChanged = value => {
    this.setState({
      value: value,
    });
  };

  async selectImage(type) {
    await ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        if (type == 'address_proof') {
          this.setState({
            address_proof: response.uri,
          });
        } else if (type == 'seller_cancel_cheque') {
          this.setState({
            seller_cancel_cheque: response.uri,
          });
        } else if (type == 'signature') {
          this.setState({
            signature: response.uri,
          });
        } else if (type == 'bannerpic') {
          this.setState({
            bannerpic: response.uri,
          });
        } else if (type == 'logopic') {
          this.setState({
            logopic: response.uri,
          });
        }
      }
    });
  }

  generatePopUp() {
    this.setState({ showPopup: true})
    }

  render() {

    let data = [
      {
        value: this.props.user.countrypic,
      },
    ];
    let businessType = [
      {
        value: this.props.user.seller_business_type,
      },
    ];
    let {bannerpic} = this.state;
    let {logopic} = this.state;
    let {seller_cancel_cheque} = this.state;
    let {address_proof} = this.state;
    let {signature} = this.state;
    let phoneNumber = 1800313000022;
    return (
      <Container>
        <Header style={styles.header}>
          <StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor="#48c0e8"
          />
          <Left>
            <Icon
              name="arrow-back"
              style={{color: '#fff'}}
              onPress={() => this.props.navigation.goBack()}
            />
          </Left>
          <Body>
            <Title style={{color: '#fff'}}>Profile</Title>
          </Body>
          {this.props.user.is_correct_info==="0" ? <Right>
						<Icon name="create" style={{ color: "white" }} onPress={() => this.edit(true)} />
					</Right> : <Right><TouchableOpacity onPress={() => this.generatePopUp()}><SvgUri
                style={styles.homesvgs}
                width="30"
                height="30"
                source={{ uri: 'https://www.hecmo.com/media/mobile/seller/lock.svg' }}
              /></TouchableOpacity></Right>}
        </Header>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showPopup}
          onRequestClose={() => {
            this.setState({ showPopup: false });
          }}>
          <View style={styles.modalPopUp}>
            <Text style={{ right:10, fontSize: 28, color: "#fff", position: "absolute" }} onPress={()=> this.setState({ showPopup: false })}>x</Text>
              <Text style={{ color: "#fff", marginTop: 30 }}>Call at: </Text>
              <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 22,marginVertical: 3 }} onPress={()=> Linking.openURL(`tel:${phoneNumber}`) }>1800 313 000 022</Text>
              <Text style={{ color: "#fff",marginVertical: 5 }}>OR </Text>
              <Text style={{ color: "#fff",marginVertical: 3 }}>Email at:</Text>
              <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 22,marginVertical: 3 }} onPress={()=> Linking.openURL('mailto:care@hecmo.com') }>care@hecmo.com</Text>
          </View>
        </Modal>


        {this.state.isVerify == 0 ? (
          <Content style={styles.contents}>
            <Form>
              <Item stackedLabel>
                <Label>Store Title</Label>
                <Input value={this.props.user.shoptitle}
                editable={false} />
              </Item>

              <Item stackedLabel>
                <Label>Contact Number</Label>
                <Input value={this.props.user.contactnumber} editable={false}/>
              </Item>

              <Item stackedLabel>
                <Label>Store Locality</Label>
                <Input value={this.props.user.complocality} editable={false}/>
              </Item>

              <Item stackedLabel>
                <Label>Facebook ID</Label>
                <Input value={this.props.user.facebookid} editable={false}/>
              </Item>
            </Form>
            <Card>
              <Text style={styles.uptxt}>Store Banner</Text>
              <Image
                source={{
                  uri: this.props.user.bannerpic+ '?nc=' + Math.random()
                }}
                style={styles.upimg}
              />
            </Card>

            <Card>
              <Text style={styles.uptxt}>Store Logo</Text>
              <Image
                source={{
                  uri: this.props.user.logopic+ '?nc=' + Math.random()
                }}
                style={styles.upimg}
              />
            </Card>

            <Form>
              <Item stackedLabel>
                <Label>Meta Keywords</Label>
                <Input value={this.props.user.meta_keyword} editable={false}/>
              </Item>
            </Form>
            <Form>
              <Item stackedLabel>
                <Label>Meta Description</Label>
                <Input value={this.props.user.meta_description} editable={false}/>
              </Item>
            </Form>

            <Text
              style={{
                textAlign: 'center',
                color: '#48c7e8',
                marginVertical: 15,
              }}>
              View Public URL
            </Text>
            <Text style={{borderBottomWidth: 4, borderColor: '#f4f4f4'}}></Text>

            <Card>
              <Text
                style={{
                  marginTop: 10,
                  color: '#48c7e8',
                  textAlign: 'center',
                  fontWeight: 'bold',
                }}>
                BUSINESS DETAILS
              </Text>
              <Form>
                <Item stackedLabel>
                  <Label>PAN</Label>
                  <Input value={this.props.user.seller_pan} editable={false}/>
                </Item>

                <Item stackedLabel>
                  <Label>Name on PAN</Label>
                  <Input value={this.props.user.seller_pan_name} editable={false}/>
                </Item>

                <Item stackedLabel>
                  <Label>Annual Turnover</Label>
                  <Input value={this.props.user.seller_annual_turnover} editable={false}/>
                </Item>

                <Item stackedLabel>
                  <Label>Business PAN</Label>
                  <Input value={this.props.user.seller_pan_business} editable={false}/>
                </Item>

                <Item stackedLabel>
                  <Label>GSTIN/Provisional No.</Label>
                  <Input value={this.props.user.seller_gstin} editable={false}/>
                </Item>
              </Form>
              <Dropdown
                data={businessType}
                label="Business Type"
                containerStyle={{
                  width: '95%',
                  marginLeft: 18,
                  marginRight: 18,
                }}
              />
            </Card>
            <Card>
              <Text style={styles.uptxt}>Cancelled Checque</Text>
              <Image
                source={{
                  uri:
                    this.props.user.seller_cancel_cheque + '?nc=' + Math.random(),
                }}
                style={{marginLeft: 10, height: 245, width: 270}}
              />
            </Card>
            <Card>
              <Text style={styles.uptxt}>Address Proof</Text>
              <Image
                source={{
                  uri: this.props.user.address_proof + '?nc=' + Math.random(),
                }}
                style={{marginLeft: 10, height: 245, width: 270}}
              />
            </Card>
            <Card>
              <Text style={styles.uptxt}>Signature</Text>
              <Image
                source={{
                  uri: this.props.user.signature + '?nc=' + Math.random(),
                }}
                style={{marginLeft: 10, height: 245, width: 270}}
              />
            </Card>
          </Content>
        ) : (
          <Content style={styles.contents}>
            <KeyboardAwareScrollView
              enableOnAndroid={true}
              style={{height: '100%'}}
              enableAutoAutomaticScroll={Platform.OS === 'android'}
              extraHeight={130}
              extraScrollHeight={130}>
              <ActivityIndicator
                animating={this.state.loader}
                style={{margin: 10, flex: 1}}
                size="large"
                color={'black'}
              />
              <Form>
                <Item stackedLabel>
                  <Label>Store Title</Label>
                  <Input
                    onChangeText={text => this.updateState(text, 'shoptitle')}>
                    {this.props.user.shoptitle}
                  </Input>
                </Item>
                <Item stackedLabel>
                  <Label>Contact Number</Label>
                  <Input
                    onChangeText={text =>
                      this.updateState(text, 'contactnumber')
                    }>
                    {this.props.user.contactnumber}
                  </Input>
                </Item>
                <Item stackedLabel>
                  <Label>Store Locality</Label>
                  <Input
                    onChangeText={text =>
                      this.updateState(text, 'complocality')
                    }>
                    {this.props.user.complocality}
                  </Input>
                </Item>
                
                <Item stackedLabel>
                  <Label>Facebook ID</Label>
                  <Input
                    onChangeText={text => this.updateState(text, 'facebookid')}>
                    {this.props.user.facebookid}
                  </Input>
                </Item>
              </Form>
              <Form>
                {/* ----------------------------Upload Store Banner--------------------------------------  */}
                <Card>
                  <Text style={styles.uptxt}>Store Banner</Text>
                  {bannerpic && (
                    <Image source={{uri: bannerpic}} style={styles.upimg} />
                  )}
                  <Button
                    onPress={() => this.selectImage('bannerpic')}
                    title="Upload Banner"
                    color="#cc9999"
                  />
                </Card>

                {/* ----------------------------Upload store logo--------------------------------------  */}
                <Card>
                  <Text style={styles.uptxt}>Store Logo</Text>
                  {logopic && (
                    <Image source={{uri: logopic}} style={styles.upimg} />
                  )}
                  <Button
                    onPress={() => this.selectImage('logopic')}
                    style={styles.upbtn}
                    title="Upload Logo"
                    color="#cc9999"
                  />
                </Card>
                <Item stackedLabel>
                  <Label>Meta Keywords</Label>
                  <Input
                    onChangeText={text =>
                      this.updateState(text, 'meta_keyword')
                    }>
                    {this.props.user.meta_keyword}
                  </Input>
                </Item>
              </Form>
              <Form>
                <Item stackedLabel>
                  <Label>Meta Description</Label>
                  <Input
                    onChangeText={text =>
                      this.updateState(text, 'meta_description')
                    }>
                    {this.props.user.meta_description}
                  </Input>
                </Item>
              </Form>

              <Card>
                <Text
                  style={{
                    marginTop: 10,
                    color: '#48c7e8',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  BUSINESS DETAIL
                </Text>
                <Form>
                  <Item>
                    <Input
                      onChangeText={text => this.updateState(text, 'selle_pan')}
                      placeholder="PAN (Director's/ Associate's/ Partner's ) ">
                      {this.props.user.seller_pan}
                    </Input>
                  </Item>
                  <Item stackedLabel>
                    <Label>Name on PAN</Label>
                    <Input
                      onChangeText={text =>
                        this.updateState(text, 'seller_pan_name')
                      }>
                      {this.props.user.seller_pan_name}
                    </Input>
                  </Item>
                  <Item stackedLabel>
                    <Label>Annual Turnover</Label>
                    <Input
                      onChangeText={text =>
                        this.updateState(text, 'seller_annual_turnover')
                      }>
                      {this.props.user.seller_annual_turnover}
                    </Input>
                  </Item>
                  <Item stackedLabel>
                    <Label>Business PAN</Label>
                    <Input
                      onChangeText={text =>
                        this.updateState(text, 'seller_pan_business')
                      }>
                      {this.props.user.seller_pan_business}
                    </Input>
                  </Item>
                  <Item stackedLabel>
                    <Label>GSTIN/Provisional No.</Label>
                    <Input
                      onChangeText={text =>
                        this.updateState(text, 'seller_gstin')
                      }>
                      {this.props.user.seller_gstin}
                    </Input>
                  </Item>

                  <Dropdown
                    label="Business Type"
                    data={businessType}
                    containerStyle={{
                      width: '95%',
                      marginLeft: 18,
                      marginRight: 18,
                    }}
                  />
                </Form>
              </Card>
              {/* ----------------------------Upload cheque card--------------------------------------  */}
              <Card>
                <Text style={styles.uptxt}>Cancelled Cheque</Text>
                {seller_cancel_cheque && (
                  <Image
                    source={{uri: seller_cancel_cheque}}
                    style={styles.upimg}
                  />
                )}
                <Button
                  onPress={() => this.selectImage('seller_cancel_cheque')}
                  title="Upload Cheque"
                  color="#cc9999"
                />
              </Card>

              {/* ----------------------------Upload address proof card--------------------------------------  */}
              <Card>
                <Text style={styles.uptxt}>Address Proof</Text>
                {address_proof && (
                  <Image
                    source={{uri: address_proof}}
                    style={{width: 100, height: 100, marginLeft: 10}}
                  />
                )}
                <Button
                  onPress={() => this.selectImage('address_proof')}
                  title="Upload Address Proof"
                  color="#cc9999"
                />
              </Card>

              {/* ----------------------------Upload signature card--------------------------------------  */}
              <Card>
                <Text style={styles.uptxt}>Signature</Text>

                {signature && (
                  <Image
                    source={{uri: signature}}
                    style={{width: 100, height: 100, marginLeft: 10}}
                  />
                )}
                <Button
                  onPress={() => this.selectImage('signature')}
                  title="Upload Signature"
                  color="#cc9999"
                />
              </Card>
            </KeyboardAwareScrollView>
            <View
              style={{marginHorizontal: 100, marginBottom: 50, marginTop: 20}}>
              <Button
                onPress={() => this.updatingProfile()}
                title="Save"
                color="#48c7e8"
              />
            </View>
          </Content>
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state, oldprops) => {
  return {
    token: state.HAReduce.token,
    user: state.HAReduce.user,
  };
};
const mapDispatchToProps = dispatch => ({
  updateToken: token => {
    dispatch(tokenValue(token));
  },
  updateUserData: data => {
    dispatch(userData(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
