import React, { PureComponent } from 'react';
import {
  View,
  ActivityIndicator,
  Button,
  TouchableHighlight,
  CheckBox,
  StatusBar,
  ScrollView
 } from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Title,
  Icon,
  Text,
  Card,
  Item,
  Form,
  Input,
  Content,
  ListItem
} from 'native-base';
import { RecyclerListView, DataProvider } from 'recyclerlistview';
import { connect } from 'react-redux';
import { DataCall } from '../utils/DataCall';
import { LayoutUtil } from '../utils/LayoutUtil';
import { UnderRMAPage } from './UnderRMAPage';
import styles from '../styles';


class RMAPage extends PureComponent {
  constructor() {
    super();
    this.state = {
      dataProvider: new DataProvider((r1, r2) => {
        return r1 !== r2;
      }),
      layoutProvider: LayoutUtil.getLayoutProvider(3),
      items: [],
      count: 0,
      viewType: 0,
      inProgressNetworkReq: false,
    };
  }

  async componentDidMount() {
    await this.fetchMoreData();
  }

  async fetchMoreData() {    
    if (!this.state.inProgressNetworkReq) {
      this.setState({ inProgressNetworkReq: true });
      const producting = await DataCall.rmaData(this.state.count, this.props.token);
      this.setState({ inProgressNetworkReq: false });
      this.setState({ isShowLoader: false });
      this.setState({
        dataProvider: this.state.dataProvider.cloneWithRows(
          producting.length>0 ? this.state.items.concat(producting) : null,
        ),
        items: this.state.items.concat(producting),
        count: this.state.count + 1,
      });
    }
  }

  rowRenderer = (type, data) => {
    return <UnderRMAPage product={data} nav={this.props} />;
  };

  handleListEnd = () => {
    this.fetchMoreData();
    this.setState({});
  };

  renderFooter = () => {
    return this.state.inProgressNetworkReq
      ? <ActivityIndicator
        style={{ margin: 10 }}
        size="large"
        color={'#48c7e8'}
      />
      : <View style={{ height: 60 }} />;
  };


render() {

    return (
      <Container>
          <Header style={{ backgroundColor: '#48c7e8' }}>
            <StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor="#48c0e8"
          />
            <Left>
                <Icon name="arrow-back" style={{color:'white'}} onPress={() => this.props.navigation.goBack()} />
            </Left>
            <Body>
              <Title>Return/Cancel</Title>
            </Body>
          </Header>    

              <RecyclerListView
              contentContainerStyle={{ margin: 3 }}
              onEndReached={this.handleListEnd}
              dataProvider={this.state.dataProvider}
              layoutProvider={this.state.layoutProvider}
              rowRenderer={this.rowRenderer}
              renderFooter={this.renderFooter}
            />


      </Container>
    );
  }
}
const mapStateToProps = (state, oldprops) => {

  return {
    ...oldprops,
    token: state.HAReduce.token,
    user: state.HAReduce.user
  };
};

export default connect(mapStateToProps)(RMAPage);
