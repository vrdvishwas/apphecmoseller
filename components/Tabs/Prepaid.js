import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  Button,
  Linking,
  ActivityIndicator
} from 'react-native';
import {Card} from 'native-base';
import { connect } from 'react-redux';
import { DataCall } from '../../utils/DataCall';
import { TouchableHighlight } from 'react-native-gesture-handler';
class Prepaid extends Component {
  constructor() {
    super();
    this.state = {
      dataSource: [],
      showLoading: false
    };
  }
  async componentDidMount() {
    this.setState({ showLoading: true });
    // Testing comment after testing
    // let home= await DataCall.TransactionList('LksIyaJe3cL5EbJSuwtI7KRL7ThwTQ8z0cTmwSdOJ4X737ZjFs3SY3t3LVnhtRMrhVly0xyDR6kwi3ga');
    
    let home= await DataCall.TransactionList(this.props.token);
    this.setState({ dataSource: home.prepaid, showLoading: false });
  }
  render() {
    return (
      <View style={{marginHorizontal: 5}}>
        {
          this.state.showLoading==true?
          <ActivityIndicator
        style={{ margin: 10 }}
        size="large"
        color={'#48c7e8'}
      />:<FlatList
          data={this.state.dataSource}
          renderItem={({item}) => (
            <Card style={{padding: 10}}>
              <View style={{ flexDirection: 'row', marginTop: 5 }}> 
              <Text style={{fontWeight: 'bold'}}>From Date</Text>
              <Text style={{ marginLeft: "30%", fontSize: 15, position: 'absolute', fontWeight: 'bold' }}>:</Text>
              <Text style={{ marginLeft: "34%", fontSize: 15, position: 'absolute' }}>{item.from_date}</Text>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={{fontWeight: 'bold'}}>To Date</Text>
                <Text style={{ marginLeft: "30%", fontSize: 15, position: 'absolute', fontWeight: 'bold' }}>:</Text>
                <Text style={{ marginLeft: "34%", fontSize: 15, position: 'absolute' }}>{item.to_date}</Text>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={{fontWeight: 'bold'}}>Amount</Text>
                <Text style={{ marginLeft: "30%", fontSize: 15, position: 'absolute', fontWeight: 'bold' }}>:</Text>
                <Text style={{ marginLeft: "34%", fontSize: 15, position: 'absolute' }}>{item.amount}</Text>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={{fontWeight: 'bold'}}>Txn Id</Text>
                <Text style={{ marginLeft: "30%", fontSize: 15, position: 'absolute', fontWeight: 'bold' }}>:</Text>
                <Text style={{ marginLeft: "34%", fontSize: 15, position: 'absolute' }}>{item.txn_id}</Text>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={{fontWeight: 'bold'}}>TXN Date</Text>
                <Text style={{ marginLeft: "30%", fontSize: 15, position: 'absolute', fontWeight: 'bold' }}>:</Text>
                <Text style={{ marginLeft: "34%", fontSize: 15, position: 'absolute' }}>{item.txn_date}</Text>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Text style={{fontWeight: 'bold'}}>Comment</Text>
                <Text style={{ marginLeft: "30%", fontSize: 15, position: 'absolute', fontWeight: 'bold' }}>:</Text>
                <Text style={{ marginLeft: "34%", fontSize: 15, position: 'absolute' }}>{item.comment}</Text>
              </View>

              <TouchableHighlight
                style={{
                  height: 40,
                  width: 120,
                  borderRadius: 20,
                  backgroundColor: '#fff',
                  marginLeft: 85,
                  marginRight: 10,
                  marginTop: 20            
                }}>
                <Button title="Download" color="#48c7e8" onPress={() => Linking.openURL(item.download_link)}/>
              </TouchableHighlight>
            </Card>
          )}
          numColumns={1}
          keyExtractor={(item, index) => index.toString()}
        />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
	return {
	  token: state.HAReduce.token,
      user: state.HAReduce.user
	};
};
export default connect(mapStateToProps)(Prepaid);
