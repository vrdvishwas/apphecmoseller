import React, { Component } from 'react';
import { Platform, StyleSheet, View, Text, Image, TouchableOpacity, Alert, AsyncStorage  } from 'react-native';

import { connect } from 'react-redux';

import { tokenValue, userData } from '../store/actions/TokenAction';
import { DataCall } from '../utils/DataCall';
import styles from '../styles';
class AuthCheck extends Component { 
    constructor(props) {
        super(props);
        this.state={
            isVisible : true
        };
        try {
        AsyncStorage.getItem('token').then((userToken) => {
      this.checkAuthorization(userToken);     
       }) } catch {
        this.props.navigation.navigate('BeforeLogin');
       }
    }

    componentDidMount(){
        var that = this;
    
        setTimeout(function(){
          that.Hide_Splash_Screen();
        }, 10000);
    }

async checkAuthorization (userToken) {
    if (userToken) {
        const profileCall= await DataCall.profileData(userToken);
        await this.props.updateToken(userToken);
    await this.props.updateUserData(profileCall);
        this.props.navigation.navigate('AfterLogin');
    }
    else {
        this.props.navigation.navigate('BeforeLogin');
    }
}

Hide_Splash_Screen=()=>{
    this.setState({ 
      isVisible : false 
    });
  }

    render() {
        let Splash_Screen = (
            <View style={styles.SplashScreen_RootView}>
                <View style={styles.SplashScreen_ChildView}>
                    <Image source={require('../assets/splash.png')}
                    style={{flex:1, width:'100%', height: '100%', resizeMode: 'cover', top:0, bottom:0, left:0, right: 0}} />
                </View>
            </View> )

        return (
            <View style = { styles.MainContainer }>
                 {(this.state.isVisible === true) ? Splash_Screen : null}
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.HAReduce.token,
        user: state.HAReduce.user
    };
};

  const mapDispatchToProps = (dispatch) => {
    return {
        updateToken: (token) => {
        dispatch(tokenValue(token));
      },
      updateUserData: (data) => {
          dispatch(userData(data));
      }
  }; 
};
export default connect(mapStateToProps, mapDispatchToProps)(AuthCheck); 