import React, { Component } from 'react';
import { StyleSheet, View, Modal } from 'react-native';

import ImageViewer from 'react-native-image-zoom-viewer';

export default class ImageZoomPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModelVisible: true,
    };
  }
  ShowModalFunction() {
    const Detailslug = this.props.navigation.getParam('dslug');
    this.setState({ isModelVisible: false });
    this.props.navigation.navigate('ProductDetail', { slug: Detailslug });
  }
  render() {
    const images = [{ url: this.props.navigation.getParam('slug'), },];
    return (
      <View style={styles.MainContainer}>
        <Modal
          visible={this.state.isModelVisible}
          transparent
          onRequestClose={() => this.ShowModalFunction()}
        >
          <ImageViewer imageUrls={images} />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: 'center',
  },
});