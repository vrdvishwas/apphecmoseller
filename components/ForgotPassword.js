import React, { Component } from 'react';
import { View, Button, Keyboard, Text, Image, StatusBar } from 'react-native';
import {
    Form,
    Item,
    Input,
    Header,
    Left,
    Icon,
    Body,
    Title, 
    Toast
} from 'native-base';

import styles from '../styles';
import { DataCall } from '../utils/DataCall';

export default class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            emailEntered: '',
            clickEvent: false,
            emailValidate: true
        };
    }
    async updateState(email) {
        await this.setState({ emailEntered: email });
        const emails = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;

            if (emails.test(email)) {
                this.setState({ emailValidate: true });
            } else {
                this.setState({ emailValidate: false });
            }
    }


    async sendEmail() {
        this.setState({ clickEvent: true });
        Keyboard.dismiss();
        const Email = await DataCall.forgotPassword(this.state.emailEntered);
        if (Email === 200) {
            Toast.show({
                text: 'Password has been succesfully sent to your email address',
                buttonText: 'Close',
                type: 'success',
                duration: 3000
              });
        }
    }

    render() {
        return (
            <View>

                <Header
                    style={{
                        backgroundColor: '#48c7e8',
                        textAlign: 'center',
                        justifyContent: 'center',
                    }}
                >
                    <StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor="#48c0e8"
          />
                    <Left>

                        <Icon style={{ color: 'white' }} onPress={() => this.props.navigation.goBack()} name="arrow-back" />

                    </Left>
                    <Body>
                        <Title>Forgot password</Title>
                    </Body>
                </Header>
				<Image style={styles.loginimages} source={require('../assets/logo.png')} />

                <View style={{ marginHorizontal: '3%',padding: 10 }}>
                    <Text style={{ marginTop: '2%' }}>Please enter your email address below. You will receive a link to reset your password.</Text>
                </View>
                <Form style={styles.formEmail}>
                    <Item floatingLabel>
                        <Input
                            placeholder="Email ID"
                            style={[this.state.emailValidate
                                ? { textAlign: 'center' }
                                : styles.loginerror]}
                            onChangeText={(text) => this.updateState(text)}
                        />
                    </Item>
                </Form>
                <View style={styles.loginbtn}>
                    <Button color="#48c7e8" title="Submit" disabled={this.state.clickEvent} onPress={() => this.sendEmail()} />
                </View>
            </View>
        );
    }
}