import React, { Component } from 'react';
import {ScrollView, Platform, StatusBar, TouchableOpacity, Modal, Linking, View } from 'react-native';
import { Header, Container, Content, Body, Left, Button, Title, Icon, Form, Item, Input, Text, Right, Label, Toast } from 'native-base';
import { connect } from 'react-redux';
import SvgUri from 'react-native-svg-uri';
import { userData, tokenValue } from '../store/actions/TokenAction';
import { DataCall } from '../utils/DataCall';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from '../styles';
class MyDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
            isVerify: 0,
            mode:false,
			payment_account_name: '',
			payment_account_no: '',
			payment_account_no_confirm: '',
			payment_account_type: '',
			payment_account_ifsc_code: '',
			payment_account_bank: '',
			payment_account_state: '',
			payment_account_city: '',
            payment_account_pincode: '',
			TextInputDisableStatus: true,
			showPopup: false
		};
	}
    
	async updatingProfile() {
		let profileData = new FormData();
		if(this.state.payment_account_name!="") {
			profileData.append('payment_account_name', this.state.payment_account_name);
		}
		if(this.state.payment_account_no!="") {
		    profileData.append('payment_account_no', this.state.payment_account_no);
		}
		if(this.state.payment_account_no_confirm!="") {
			profileData.append('payment_account_no_confirm', this.state.payment_account_no_confirm);
		}
		if(this.state.payment_account_type!="") {
			profileData.append('payment_account_type', this.state.payment_account_type);
		}
		if(this.state.payment_account_ifsc_code!="") {
			profileData.append('payment_account_ifsc_code', this.state.payment_account_ifsc_code);
		}
		if(this.state.payment_account_bank!="") {
		profileData.append('payment_account_bank', this.state.payment_account_bank);
		}
		if(this.state.payment_account_state!="") {
		profileData.append('payment_account_state', this.state.payment_account_state);
		}
		if(this.state.payment_account_city!="") {
		profileData.append('payment_account_city', this.state.payment_account_city);
		}
		if(this.state.payment_account_pincode!="") {
		profileData.append('payment_account_pincode', this.state.payment_account_pincode);
		}

		console.log("profileData profileData ", profileData);
		const ds = await DataCall.updateProfile(profileData, this.props.token);
		Toast.show({
			text: ds.message,
			buttonText: 'Close',
			type: 'success',
			duration: 3000
		  });
		  const rs=await DataCall.profileData(this.props.token);
          await this.props.updateUserData(rs);
		  this.props.navigation.goBack();
	}

	async updateState(text, type) {
		if (type == 'payment_account_name') {
			this.setState({ payment_account_name: text });
		}
		if (type == 'payment_account_no') {
			this.setState({ payment_account_no: text });
		}
		if (type == 'payment_account_no_confirm') {
			this.setState({ payment_account_no_confirm: text });
		}
		if (type == 'payment_account_type') {
			this.setState({ payment_account_type: text });
		}
		if (type == 'payment_account_ifsc_code') {
			this.setState({ payment_account_ifsc_code: text });
		}
		if (type == 'payment_account_bank') {
			this.setState({ payment_account_bank: text });
		}
		if (type == 'payment_account_state') {
			this.setState({ payment_account_state: text });
		}
		if (type == 'payment_account_city') {
			this.setState({ payment_account_city: text });
		}
		if (type == 'payment_account_pincode') {
			this.setState({ payment_account_pincode: text });
		}
	}

	edit(mode) {
		if (mode == true) {
      this.setState({ isVerify: 1 });
      this.setState({ TextInputDisableStatus: false });
		} else {
			this.setState({ isVerify: 0 });
    }
      }  

	  generatePopUp() {
		this.setState({ showPopup: true})
		}

	render() {
		let phoneNumber = 1800313000022;
		return (
			<Container>
				<Header style={styles.header}>
				<StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor="#48c0e8"
          />
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" />
						</Button>
					</Left>
					<Body>
						<Title>Payment Details</Title>
					</Body>
					{this.props.user.is_correct_info==="0" ? <Right>
						<Icon name="create" style={{ color: "white" }} onPress={() => this.edit(true)} />
					</Right> : <Right><TouchableOpacity onPress={() => this.generatePopUp()}><SvgUri
                style={styles.homesvgs}
                width="30"
                height="30"
                source={{ uri: 'https://www.hecmo.com/media/mobile/seller/lock.svg' }}
              /></TouchableOpacity></Right>}
				</Header>

				<Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showPopup}
          onRequestClose={() => {
            this.setState({ showPopup: false });
          }}>
          <View style={styles.modalPopUp}>
            <Text style={{ right:10, fontSize: 28, color: "#fff", position: "absolute" }} onPress={()=> this.setState({ showPopup: false })}>x</Text>
              <Text style={{ color: "#fff", marginTop: 30 }}>Call at: </Text>
              <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 22,marginVertical: 3 }} onPress={()=> Linking.openURL(`tel:${phoneNumber}`) }>1800 313 000 022</Text>
              <Text style={{ color: "#fff",marginVertical: 5 }}>OR </Text>
              <Text style={{ color: "#fff",marginVertical: 3 }}>Email at:</Text>
              <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 22,marginVertical: 3 }} onPress={()=> Linking.openURL('mailto:care@hecmo.com') }>care@hecmo.com</Text>
          </View>
        </Modal>

				{this.state.isVerify == 0 ?
					<Content style={styles.contents}>
						<Form>
							<Item stackedLabel>
								<Label>Account Holder Name</Label> 
								<Input value={this.props.user.payment_account_name} editable={false}/>
							</Item>
							<Item stackedLabel>
								<Label>Account No</Label>
								<Input value={this.props.user.payment_account_no} editable={false}/>
							</Item>
							<Item stackedLabel>
								<Label>Confirm Account No</Label>
								<Input
									value={this.props.user.payment_account_no_confirm}
									editable={false} />
							</Item>
							<Item stackedLabel>
								<Label>Payment Type</Label>
								<Input value={this.props.user.payment_account_type} editable={false} />
							</Item>
							<Item stackedLabel>
								<Label>Bank IFSC code</Label>
								<Input value={this.props.user.payment_account_ifsc_code} editable={false}/>
							</Item>
							<Item stackedLabel>
								<Label>Bank Name</Label>
								<Input value={this.props.user.payment_account_bank} editable={false}/>
							</Item>
							<Item stackedLabel>
								<Label>Bank State</Label>
								<Input value={this.props.user.payment_account_state} editable={false}/>
							</Item>
							<Item stackedLabel>
								<Label>Bank City</Label>
								<Input value={this.props.user.payment_account_city} editable={false}/>
							</Item>
							<Item stackedLabel>
								<Label>Bank Pincode</Label>
								<Input value={this.props.user.payment_account_pincode} editable={false}/>
							</Item>
						</Form>
					</Content> : <Content style={styles.contents}>
  		 <KeyboardAwareScrollView enableOnAndroid={true} style={{height:"100%"}}
            enableAutoAutomaticScroll={(Platform.OS === 'android')} extraHeight={130} extraScrollHeight={130}>
							<Form>
							<Item stackedLabel>
								<Label>Account Holder Name</Label>
								<Input
									onChangeText={(text) => this.updateState(text, 'payment_account_name')}
				>{this.props.user.payment_account_name}</Input>
							</Item>
							<Item stackedLabel>
								<Label>Account No</Label>
								<Input
									onChangeText={(text) => this.updateState(text, 'payment_account_no')}
								>{this.props.user.payment_account_no}</Input>
							</Item>
							<Item stackedLabel>
								<Label>Confirm Account No</Label>
								<Input
									onChangeText={(text) => this.updateState(text, 'payment_account_no_confirm')}
								>{this.props.user.payment_account_no_confirm}</Input>
							</Item>
							<Item stackedLabel>
								<Label>Payment Type</Label>
								<Input
									onChangeText={(text) => this.updateState(text, 'payment_account_type')}
								>{this.props.user.payment_account_type}</Input>
							</Item>
							<Item stackedLabel>
								<Label>Bank IFSC code</Label>
								<Input
									onChangeText={(text) => this.updateState(text, 'payment_account_ifsc_code')}
								>{this.props.user.payment_account_ifsc_code}</Input>
							</Item>
							<Item stackedLabel>
								<Label>Bank Name</Label>
								<Input
									onChangeText={(text) => this.updateState(text, 'payment_account_bank')}
								>{this.props.user.payment_account_bank}</Input>
							</Item>
							<Item stackedLabel>
								<Label>Bank State</Label>
								<Input
									onChangeText={(text) => this.updateState(text, 'payment_account_state')}
								>{this.props.user.payment_account_state}</Input>
							</Item>
							<Item stackedLabel>
								<Label>Bank City</Label>
								<Input
									onChangeText={(text) => this.updateState(text, 'payment_account_city')}
								>{this.props.user.payment_account_city}</Input>
							</Item>
							<Item stackedLabel>
								<Label>Bank Pincode</Label>
								<Input
                	onChangeText={(text) => this.updateState(text, 'payment_account_pincode')}
								>{this.props.user.payment_account_pincode}</Input>
							</Item>
						</Form>
						<Button info style={styles.shippingbtn} onPress={() => this.updatingProfile()}>
							<Text style={styles.savetxt}>SAVE</Text>
						</Button>
            </KeyboardAwareScrollView>
            					</Content>
                    }
			</Container>
		);
	}
}

const mapStateToProps = (state, oldprops) => {
	return {
		token: state.HAReduce.token,
		user: state.HAReduce.user
	};
};

const mapDispatchToProps = (dispatch) => ({
    updateUserData: (data) => {
      dispatch(userData(data));
    }
  }); 

export default connect(mapStateToProps,mapDispatchToProps)(MyDetails);
