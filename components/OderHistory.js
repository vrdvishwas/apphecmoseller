import React, {PureComponent} from 'react';
import {
  View,
  ActivityIndicator,
  TouchableHighlight,
  CheckBox,
  Button,
  StatusBar
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Title,
  Icon,
  Text,
  Card,
  Item,
  Form,
  Input,
  Content,
  ListItem
} from 'native-base';
import {RecyclerListView, DataProvider} from 'recyclerlistview';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import {DataCall} from '../utils/DataCall';
import {LayoutUtil} from '../utils/LayoutUtil';
import DatePicker from 'react-native-datepicker';
import {Dropdown} from 'react-native-material-dropdown';
import {UnderOrderHistory} from './UnderOrderHistory';
import styles from '../styles';
class OrderHistory extends PureComponent {
  constructor() {
    super();
    this.state = {
      dataProvider: new DataProvider((r1, r2) => {
        return r1 !== r2;
      }),
      layoutProvider: LayoutUtil.getLayoutProvider(2),
      items: [],
      count: 1,
      viewType: 0,
      inProgressNetworkReq: false,
      isModalVisible: false,
      fromdate: '',
      todate: '',
      orderstatus: '',
      isSortVisible: false,
      name: false,
      price: false,
      quantity: false,
      date: false,
      searchKeyword: ''
    };
  }
  setModalVisible = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };

  sortModalVisible = () => {
    this.setState({isSortVisible: !this.state.isSortVisible});
  };

  componentWillMount() {
    this.fetchMoreData();
  }

  async filterData() {
    await this.setState({
      dataProvider: new DataProvider((r1, r2) => {
        return r1 !== r2;
      }),
      // layoutProvider: LayoutUtil.getLayoutProvider(2),
      items: [],
      count: 1,
      viewType: 0,
      inProgressNetworkReq: false,
      isModalVisible: false,
    });
    await this.fetchMoreData();
  }

  // async sortData() {
  //   await this.setState({
  //     dataProvider: new DataProvider((r1, r2) => {
  //       return r1 !== r2;
  //     }),
  //     layoutProvider: LayoutUtil.getLayoutProvider(2),
  //     items: [],
  //     count: 0,
  //     viewType: 0,
  //     inProgressNetworkReq: false,
  //     isModalVisible: false,
  //   });
  //   this.fetchMoreData();
  // }

  async fetchMoreData() {
    if (!this.state.inProgressNetworkReq) {
      this.setState({inProgressNetworkReq: true});
      console.log('state values', this.state);
      const producting = await DataCall.orderHistoryData(
        this.state.count,
        10,
        this.props.token,
        // 'LksIyaJe3cL5EbJSuwtI7KRL7ThwTQ8z0cTmwSdOJ4X737ZjFs3SY3t3LVnhtRMrhVly0xyDR6kwi3ga',
        this.state.fromdate,
        this.state.todate,
        this.state.orderstatus,
        this.state.searchKeyword,
        this.state.cancelStatus,
      );
      console.log('filtered data', producting);
      this.setState({inProgressNetworkReq: false});
      this.setState({
        dataProvider: this.state.dataProvider.cloneWithRows(
          producting.length>0 ? this.state.items.concat(producting) : null,          
        ),
        items: this.state.items.concat(producting),
        count: this.state.count + 1,
      });
    }
  }

  rowRenderer = (type, data) => {
    return <UnderOrderHistory product={data} nav={this.props} />;
  };

  viewChangeHandler = viewType => {
    this.setState({
      layoutProvider: LayoutUtil.getLayoutProvider(viewType),
      viewType: viewType,
    });
  };

  handleListEnd = () => {
    this.fetchMoreData();
    this.setState({});
  };

  renderFooter = () => {
    return this.state.inProgressNetworkReq ? (
      <ActivityIndicator style={{margin: 10}} size="large" color={'#48c7e8'} />
    ) : (
      <View style={{height: 60}} />
    );
  };

  updateOrderStatus(value) {
    this.setState({
      orderstatus: value,
    });
  }

  updateOrderCancel(value) {
    this.setState({
      cancelStatus: value,
    });
  }

  updateState(text) {
    this.setState({
      searchKeyword: text 
    });  
    }

  async clearAllFilters(){
      await this.setState({
      searchKeyword: '',
      fromdate: '',
      todate: '',
      orderstatus: '',
      cancelStatus: ''
      });
  }

  render() {
    let data = [
      {value:'',label: 'All'},
      {value:'upcoming',label: 'Upcoming'},
      {value:'pending',label: 'Pending'},
      {value:'processing',label: 'Processing'},
      {value:'complete',label: 'Complete'},
      {value:'canceled',label: 'Cancel'},
      {value:'ready_to_dispatch',label: 'Ready to dispatch'},
      {value:'holded',label: 'Holded'},
      {value:'pickup_ready',label: 'Pickup ready'},
    ];

    let cancelby = [
      {
        value: '',
        label: '--'
      },
      {
        value: '0',
        label: 'Customer'
      },
      {
        value: '1', 
        label: 'You'
      },
    ];

    // const token ='LksIyaJe3cL5EbJSuwtI7KRL7ThwTQ8z0cTmwSdOJ4X737ZjFs3SY3t3LVnhtRMrhVly0xyDR6kwi3ga';
    return (
      <Container>
        {this.props.navigation ? (
          <Header
            style={{backgroundColor: '#48c7e8', justifyContent: 'center'}}>
               <StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor="#48c0e8"
          />
            <Left>
              <Icon
                style={{color: '#fff'}}
                name="arrow-back"
                onPress={() => this.props.navigation.goBack()}
              />
            </Left>
            <Body>
              <Title>Order History</Title>
            </Body>
          </Header>
        ) : null}
        <Card
          style={{
            alignItems: 'center',
            flexDirection: 'row',
            height: '8%',
            width: '100%',
          }}>
      
          <Text
            onPress={this.setModalVisible}
            style={{
              fontStyle: 'normal',
              fontWeight: 'normal',
              right: 0,
              width: '100%',
              textAlign: 'center',
            }}>
            &#x2191;&#x2193;Filter
          </Text>
        </Card>
        {/* {this.state.items.length > 0 ? ( */}
          <RecyclerListView
            contentContainerStyle={{margin: 3}}
            onEndReached={this.handleListEnd}
            dataProvider={this.state.dataProvider}
            layoutProvider={this.state.layoutProvider}
            rowRenderer={this.rowRenderer}
            renderFooter={this.renderFooter}
          />
        {/* ) : null}  */}

        <Modal
        style={styles.bottomModal}
          visible={this.state.isModalVisible}>
          <Content style={styles.modalContent}>

            <Icon name="arrow-back" style={{color:"#000", margin: 15 }} onPress={() => {this.setModalVisible(!this.state.isModalVisible); }}/>
            <Text onPress={() => {this.clearAllFilters(); }} style={{ color: "blue", position: "absolute", right: 15, marginTop: 20 }}>Clear Filters</Text>
            <Form>
              <Item style={{ marginHorizontal: 15 }}>
                <Input placeholder="Search by order ID" onChangeText={(text) => this.updateState(text)}></Input>
              </Item>
              <DatePicker
                style={styles.datepicker}
                date={this.state.fromdate}
                mode="date"
                placeholder="From Date"
                format="YYYY-MM-DD"
                minDate="2016-05-01"
                maxDate="2020-12-31"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 10,
                    top: 4,
                    fontSize: 20,
                    marginLeft: 0,
                  },
                  dateInput: {
                    marginLeft: 13,
                    marginRight: 13,
                    borderTopWidth: 0,
                    borderLeftWidth: 0,
                    borderRightWidth: 0,
                  },
                }}
                onDateChange={date => {
                  this.setState({fromdate: date});
                }}
              />

              <DatePicker
                style={styles.datepicker}
                date={this.state.todate}
                mode="date"
                placeholder="To Date"
                format="YYYY-MM-DD"
                minDate="2016-05-01"
                maxDate="2030-12-31"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 10,
                    top: 4,
                    fontSize: 20,
                    marginLeft: 0,
                  },
                  dateInput: {
                    marginLeft: 13,
                    marginRight: 13,
                    borderTopWidth: 0,
                    borderLeftWidth: 0,
                    borderRightWidth: 0,
                  },
                }}
                onDateChange={dates => {
                  this.setState({todate: dates});
                }}
              />
<View style={{ marginHorizontal: 15 }}>
              <Dropdown
                data={data}
                label="Order Status"
                onChangeText={(value, index, data) =>
                  this.updateOrderStatus(value, index, data)
                }
                value={this.state.orderstatus}
              />
              </View>

<View style={{ marginHorizontal: 15 }}>
              <Dropdown
                data={cancelby}
                value={this.state.cancelStatus}
                label="Cancelled By"
                // valueExtractor={({ value }) => value}
                // labelExtractor={({ label }) => label}
                onChangeText={(value, index, data) =>
                  this.updateOrderCancel(value, index, data)
                }
              />
              </View>
            </Form>
          </Content>
          <View style={{ flexDirection: 'row', width: "100%"}}>
            <View style={{ width: "45%", height: 60, marginHorizontal: 10 }}>
            <Button
                onPress={() => {
                  this.setModalVisible(!this.state.isModalVisible);
                }}
                color="#808080"
                title="Cancel"
              />
            </View>
            <View style={{ width: "45%", height: 60 }}>
              <Button
                onPress={() => {
                  this.filterData();
                }}
                color="#48c7e8"
                title="Apply"
              />
              </View>
          </View>
        </Modal>

        {/* -------------------------------------- sort Modal----------------------- */}

       
{/* <Modal
          isVisible={this.state.isSortVisible}
          style={styles.bottomModal}
          animationInTiming={500}
          animationOutTiming={500}
        >
            <Content styles={ styles.modalContent }>
              <Icon name="arrow-back" onPress={this.sortModalVisible} style={{ color: "#000", margin: 15 }}/>
              <ListItem>
                <CheckBox value={this.state.ascendingName} color='#48c7e8' onValueChange={() => this.ascendingName()} />
                <Body>
                  <Text>Name: A - Z</Text>
                </Body>
              </ListItem>
              <ListItem>
                <CheckBox value={this.state.descendingName} color='#48c7e8' onValueChange={() => this.descendingName()} />
                <Body>
                  <Text>Name: Z - A</Text>
                </Body>
              </ListItem>

              <ListItem>
                <CheckBox value={this.state.ascendingCommission} color='#48c7e8' onValueChange={() => this.ascendingCommission()} />
                <Body>
                  <Text>Price: Low - High</Text>
                </Body>
              </ListItem>
              <ListItem>
                <CheckBox value={this.state.descendingCommission} color='#48c7e8' onValueChange={() => this.descendingCommission()} />
                <Body>
                  <Text>Price: High - Low</Text>
                </Body>
              </ListItem>

              <ListItem>
                <CheckBox value={this.state.ascendingCommission} color='#48c7e8' onValueChange={() => this.ascendingCommission()} />
                <Body>
                  <Text>Quantity: Min - Max</Text>
                </Body>
              </ListItem>
              <ListItem>
                <CheckBox value={this.state.descendingCommission} color='#48c7e8' onValueChange={() => this.descendingCommission()} />
                <Body>
                  <Text>Quantity: Max - Min</Text>
                </Body>
              </ListItem>

              <ListItem>
                <CheckBox value={this.state.ascendingPrice} color='#48c7e8' onValueChange={() => this.ascendingPrice()} />
                <Body>
                  <Text>Date: Ascending</Text>
                </Body>
              </ListItem>

              <ListItem>
                <CheckBox value={this.state.descendingPrice} color='#48c7e8' onValueChange={() => this.descendingPrice()} />
                <Body>
                  <Text>Date: Descending</Text>
                </Body>
              </ListItem>

              <View style={{ flexDirection: 'row', width: "100%" }}>
                <View style={{ height: 60, width: "45%", marginHorizontal: 10 }}>
                <Button title="Cancel"  onPress={this.sortModalVisible}  color="#48c7e8" />
                </View>
              <View style={{ height: 60, width: "45%" }}>
              <Button title="Apply"  onPress={this.sortModalVisible} color="#48c7e8" />
              </View>
              
            </View>

            </Content>
        </Modal>   */}
      </Container>
    );
  }
}
const mapStateToProps = (state, oldprops) => {
  return {
    ...oldprops,
    token: state.HAReduce.token,
    user: state.HAReduce.user,
  };
};

export default connect(mapStateToProps)(OrderHistory);
