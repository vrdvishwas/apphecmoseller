/* eslint-disable react/jsx-no-undef */
import React, { Component } from 'react';
import { Image, View, TouchableHighlight, StyleSheet } from 'react-native';
// eslint-disable-next-line max-len
import { Container, Header, Content, Left, Body, Text, Title, Button, Tab, Tabs, Icon, Item ,Card, Form, Input} from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';


export default class ReturnView extends Component {
  render() {

    let data = [ 
        {value: '5',
    }, {
      value: '10',
    },
    {value:'20'},
    {value:'all'},
    ];
    return (
      <Container>
         <Header
                 style={{
                      backgroundColor: '#48c7e8',
                      textAlign: 'center',
                      justifyContent: 'center',
                    }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>My Requested RMA Details</Title>
                    </Body>
                </Header>
        <Content>
        <Card style={styles.carddeta}>
            <Text style={styles.text}>Order Id</Text>
            <Text style={styles.nortxt}>#100000146</Text>
            </Card>
            <Text>Status</Text>
   
            <Card style={styles.carddeta}>
            <Text style={styles.text}>RMA STATUS</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Solved</Text>
            <Text style={styles.text}>MY STATUS</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Pending</Text>
            <Text style={styles.text}>Status(CUSTOMER)</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Delivered</Text>
            <Text style={styles.text}>Consignment Number:</Text>
            <Text numberOfLines={1} style={styles.nortxt}></Text>
            <Text style={styles.text}>Requested Quantity to Cancel:</Text>
            <Text numberOfLines={1} style={styles.nortxt}>1</Text>
            </Card>
            <Card style={styles.carddeta}>
            <Text style={styles.text}>REASON</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Item would not arrive on time</Text>
            </Card>
            <Card style={styles.carddeta}>
            <Text style={styles.text}>Additional Information</Text>
            <Text numberOfLines={1} style={styles.nortxt}>testing</Text>
            </Card>
            <Text>Uploaded Product Image(S)</Text>
        <Button success style={styles.btn}><Text numberOfLines={1} style={styles.btntext}>CONFIRM</Text></Button>
        <Text>Item(s) Requested for RMA</Text>
        <Card style={styles.carddeta}>
            <Text style={styles.text}>PRODUCT NAME</Text>
            <Text numberOfLines={2} style={styles.nortxt}>St. Ives Imported Even And Bright, Pink Lemon & Mandarin Body Wash (400ml-Pack of 2)</Text>
            <Text style={styles.text}>SKU</Text>
            <Text numberOfLines={1} style={styles.nortxt}>BLULP545</Text>
            <Text style={styles.text}>PRICE</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Rs.150.00</Text>
            <Text style={styles.text}>TAX</Text>
            <Text numberOfLines={1} style={styles.nortxt}>0.0000</Text>
            <Text style={styles.text}>QTY</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Ordered: 1</Text>
            <Text style={styles.text}>SUBTOTAL</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Rs.150.00</Text>
        </Card>
 
        <Card style={styles.carddeta}>
        <View style={{flex:1, flexDirection:'row',backgroundColor:'#70C85C'}}>
        <Text>27/May/2018 4:54:39 pm</Text>
        <Text style={{marginLeft:20}}>Seller: Vishal Chanana</Text>
        </View>
        <Text>testing</Text>
        </Card>
          <View style={{flex:1,flexDirection:'row',height:100,marginTop:20}}>
            <Text>1 item(s)</Text>
            <Text> SHOW: </Text>
            <Dropdown
               data={data}
               containerStyle={{
               width: "40%",position:'relative',left:10,bottom:33}}/>
          </View>
        </Content>
      </Container>
    );
  }
}
const styles=StyleSheet.create({
  nortxt:{marginLeft:5},
  text:{fontWeight:'bold',marginLeft:5},
  vieworder:{color:'blue',marginLeft:4,fontSize:18,marginTop:3,marginBottom:5},
  carddeta:{width:348,marginTop:10,marginLeft:5,marginRight:10},
  btn:{width:142,height:20,marginLeft:2,marginTop:5,marginRight:70,},
  btntext:{fontSize:12,textAlign:'center'},

});