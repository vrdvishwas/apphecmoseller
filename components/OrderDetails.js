/* eslint-disable max-len */
import React, { Component } from 'react';
import {
  View,
  Button,
  StatusBar,
  Linking
} from 'react-native';
import {TouchableHighlight, TouchableOpacity} from 'react-native-gesture-handler';
import SvgUri from 'react-native-svg-uri';
import { connect } from 'react-redux';
import { DataCall } from '../utils/DataCall';
import { Container, Header, Card, Icon, Left, Body, Title, Text, Content, Right } from 'native-base';
import styles from '../styles';
class ProductDetails extends Component {
  
	constructor() {
		super();
		this.state = {
      id: '',
      detail:{
        items: []
      },
      address:'',
      showLoader: false,
      dispatchText: "READY TO DISPATCH"
    };
    this.dispatchButton= this.dispatchButton.bind(this);
	}

	async componentDidMount(){
    this.showData();

}

async showData() {
  console.log(this.props.navigation.getParam('slug'));
  this.setState({ showLoader: true });
  let orderText= await DataCall.orderDetailsData(this.props.navigation.getParam('slug'), this.props.token);
  this.setState({ detail: orderText, showLoader: false });
 }

 async dispatchButton(){
  let buttonAction= await DataCall.readyToDispatchButtonAction(this.props.navigation.getParam('slug'), this.props.token);
  if(buttonAction==200) {
    this.setState({ dispatchText: "DISPATCH REQUESTED" });
  }
}

  render() {
    // shipping_print
    // invoice_print
    // https://www.hecmo.com/media/mobile/seller/My_Details.svg
    // https://www.hecmo.com/media/mobile/seller/Shipping.svg
    return (
      <Container>
        <Header style={{backgroundColor:'#48c7e8'}}>
        <StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor="#48c0e8"
          />
                    <Left>
                            <Icon style={{color:'#fff'}} onPress={() => this.props.navigation.goBack()} name="arrow-back" />
                                  </Left>
                    <Body>
                        <Title>Order Detail</Title>
                    </Body>
                    
                    {/* <Right>
                    {this.state.detail.shipping_print=="0"?null:<TouchableOpacity onPress={() => Linking.openURL(this.state.detail.shipping_print)}>
                      <SvgUri
							style={{ marginRight: 6, marginLeft: 30, color: "#fff" }}
							width="26"
              height="26"
							source={{ uri: 'https://www.hecmo.com/media/mobile/seller/Shipping_white.svg' }}
						/></TouchableOpacity>}

                    {this.state.detail.invoice_print=="0"?null:<TouchableOpacity onPress={() => Linking.openURL(this.state.detail.invoice_print)}>
                      <SvgUri
							style={{ marginRight: 6, marginLeft: 30, color: "#fff" }}
							width="26"
              height="26"
							source={{ uri: 'https://www.hecmo.com/media/mobile/seller/My_Details_white.svg' }}
						/></TouchableOpacity>}
                    </Right> */}
                </Header>
                <Content>
                  <Card style={styles.cardorder}>
      
      {this.state.detail.ready_to_dispatch=="0" && this.state.detail.show_ready_to_dispatch==true ?<View style={{ alignContent: "center", marginBottom: 20 }}>
    {this.state.dispatchText=="READY TO DISPATCH"?<Button color="green" onPress={() => this.dispatchButton()} title={this.state.dispatchText} />:<Text style={{ textAlign: "center", color: "green", fontSize: 25 }}>{this.state.dispatchText}</Text>}    
</View>:null}

                  <Text style={styles.heading}>Order #{this.state.detail.order_id}</Text>
                  {this.state.detail.items.map((person, index) => (
      <View key={index}>
        <Text style={{marginLeft:10,marginTop:10,marginBottom:10}}>{person.name}</Text>
 </View>
    ))}
                      <Text style={{marginLeft:10}}>Date: {this.state.detail.order_date}</Text>
                    <Text style={[styles.heading,{marginTop:10}]}>Buyer Information</Text>
                    <View style={{flex:1,flexDirection:'row',marginTop:10,marginBottom:10}}>
                    <Text style={styles.orderText}>Name: </Text>
                    <Text>{this.state.detail.customer_name}</Text>
                    </View>

                    {/* <View style={{flex:1,flexDirection:'row',marginTop:10,marginBottom:10}}>
                    <Text style={[styles.orderText]}>Email: </Text>
                    <Text>{this.state.detail.customer_email}</Text>
                    </View> */}
  
                    <Text style={[styles.orderText,{marginTop:10,marginBottom:10, fontWeight: 'bold' }]}>Shipping Address: </Text>
                    <View style={{ flexDirection: 'row',marginHorizontal: 5, borderBottomWidth: 2,borderBottomColor: "#f4f4f4" }}>
                    <Text style={{ marginHorizontal: 5 }}>Name</Text>
                    <Text style={{ position: 'absolute', marginLeft: "40%" }}>:</Text>
                    <Text style={{ position: 'absolute', marginLeft: "47%" }}>{this.state.detail.shipping_name}</Text>
                    </View>

                    {/* <View style={{ flexDirection: 'row',marginHorizontal: 5, borderBottomWidth: 2,borderBottomColor: "#f4f4f4" }}>
                    <Text style={{ marginHorizontal: 5 }}>Address</Text>
                    <Text style={{ position: 'absolute', marginLeft: "40%" }}>:</Text>
                    <Text style={{ position: 'absolute', marginLeft: "47%" }}>{this.state.detail.shipping_address_1}</Text>
                    </View> */}

                    <View style={{ flexDirection: 'row',marginHorizontal: 5, borderBottomWidth: 2,borderBottomColor: "#f4f4f4" }}>
                    <Text style={{ marginHorizontal: 5 }}>State</Text>                      
                    <Text style={{ position: 'absolute', marginLeft: "40%" }}>:</Text>
                    <Text style={{ position: 'absolute', marginLeft: "47%" }}>{this.state.detail.shipping_state}</Text>
                    </View>

                    <View style={{ flexDirection: 'row',marginHorizontal: 5, borderBottomWidth: 2,borderBottomColor: "#f4f4f4" }}>
                    <Text style={{ marginHorizontal: 5 }}>City</Text>
                    <Text style={{ position: 'absolute', marginLeft: "40%" }}>:</Text>
                    <Text style={{ position: 'absolute', marginLeft: "47%" }}>{this.state.detail.shipping_city}</Text>
                    </View>

                    <View style={{ flexDirection: 'row',marginHorizontal: 5, borderBottomWidth: 2,borderBottomColor: "#f4f4f4" }}>
                    <Text style={{ marginHorizontal: 5 }}>Country</Text>
                    <Text style={{ position: 'absolute', marginLeft: "40%" }}>:</Text>
                    <Text style={{ position: 'absolute', marginLeft: "47%" }}>{this.state.detail.shipping_country}</Text>
                    </View>

                    {/* <View style={{ flexDirection: 'row',marginHorizontal: 5, borderBottomWidth: 2,borderBottomColor: "#f4f4f4" }}>
                    <Text style={{ marginHorizontal: 5 }}>Phone</Text>
                    <Text style={{ position: 'absolute', marginLeft: "40%" }}>:</Text>
                    <Text style={{ position: 'absolute', marginLeft: "47%" }}>{this.state.detail.shipping_phone}</Text>
                    </View> */}

                    
                    <Text style={[styles.orderText,{marginTop:10,marginBottom:10, fontWeight: 'bold'}]}>Billing Address: </Text>
                    
                    <View style={{ flexDirection: 'row',marginHorizontal: 5, borderBottomWidth: 2,borderBottomColor: "#f4f4f4" }}>
                    <Text style={{ marginHorizontal: 5 }}>Name</Text>
                    <Text style={{ position: 'absolute', marginLeft: "40%" }}>:</Text>
                    <Text style={{ position: 'absolute', marginLeft: "47%" }}>{this.state.detail.billing_name}</Text>
                    </View>

                    {/* <View style={{ flexDirection: 'row',marginHorizontal: 5, borderBottomWidth: 2,borderBottomColor: "#f4f4f4" }}>
                    <Text style={{ marginHorizontal: 5 }}>Address</Text>
                    <Text style={{ position: 'absolute', marginLeft: "40%" }}>:</Text>
                    <Text style={{ position: 'absolute', marginLeft: "47%" }}>{this.state.detail.billing_address_1}</Text>
                    </View> */}

                    <View style={{ flexDirection: 'row',marginHorizontal: 5, borderBottomWidth: 2,borderBottomColor: "#f4f4f4" }}>
                    <Text style={{ marginHorizontal: 5 }}>State</Text>
                    <Text style={{ position: 'absolute', marginLeft: "40%" }}>:</Text>
                    <Text style={{ position: 'absolute', marginLeft: "47%" }}>{this.state.detail.billing_state}</Text>
                    </View>

                    <View style={{ flexDirection: 'row',marginHorizontal: 5, borderBottomWidth: 2,borderBottomColor: "#f4f4f4" }}>
                    <Text style={{ marginHorizontal: 5 }}>City</Text>
                    <Text style={{ position: 'absolute', marginLeft: "40%" }}>:</Text>
                    <Text style={{ position: 'absolute', marginLeft: "47%" }}>{this.state.detail.billing_city}</Text>
                    </View>

                    <View style={{ flexDirection: 'row',marginHorizontal: 5, borderBottomWidth: 2,borderBottomColor: "#f4f4f4" }}>
                    <Text style={{ marginHorizontal: 5 }}>Country</Text>
                    <Text style={{ position: 'absolute', marginLeft: "40%" }}>:</Text>
                    <Text style={{ position: 'absolute', marginLeft: "47%" }}>{this.state.detail.billing_country}</Text>
                    </View>

                    {/* <View style={{ flexDirection: 'row',marginHorizontal: 5, borderBottomWidth: 2,borderBottomColor: "#f4f4f4" }}>
                    <Text style={{ marginHorizontal: 5 }}>Phone</Text>
                    <Text style={{ position: 'absolute', marginLeft: "40%" }}>:</Text>
                    <Text style={{ position: 'absolute', marginLeft: "47%" }}>{this.state.detail.billing_phone}</Text>
                    </View> */}
                    {/* <Text style={{marginTop:10,marginBottom:10}}>{this.state.detail.shipping_method}</Text> */}

                    <Text style={styles.heading}>Payment Method</Text>
                    <Text style={styles.orderText}>{this.state.detail.payment_method}</Text>
                    <View style={{flex:1,flexDirection:'row'}}>
                      <Text style={[styles.font,{marginBottom:10,marginTop:10}]}>   Order Total:</Text>
                      <Text style={[styles.font,{marginBottom:20,marginTop:10}]}> {this.state.detail.total}</Text>
                  </View>

            <View >
              <View style={{ marginVertical: 10 }}>
                  {this.state.detail.shipping_print=="0"?null:<TouchableOpacity style={{ flexDirection: "row" }} onPress={() => Linking.openURL(this.state.detail.shipping_print)}>
                      <SvgUri
							style={{ marginRight: 6, marginLeft: 30, color: "#fff" }}
							width="46"
              height="46"
							source={{ uri: 'https://www.hecmo.com/media/mobile/seller/Shipping.svg' }}
						/>
            <Text style={{ marginHorizontal: 10, color: "blue", marginTop: 8 }}>Print/Download</Text>
            </TouchableOpacity>}


              </View>
              <View style={{ marginVertical: 10 }} >
                    {this.state.detail.invoice_print=="0"?null:<TouchableOpacity style={{ flexDirection: "row" }} onPress={() => Linking.openURL(this.state.detail.invoice_print)}>
                      <SvgUri
							style={{ marginRight: 6, marginLeft: 30, color: "#fff" }}
							width="46"
              height="46"
							source={{ uri: 'https://www.hecmo.com/media/mobile/seller/My_Details.svg' }}
						/>
            <Text style={{ marginHorizontal: 10, color: "blue", marginTop: 10 }}>Print/Download</Text>
            </TouchableOpacity>}
            
            </View>

            <View style={{ marginVertical: 10 }}>
                    {this.state.detail.manifest_print=="0"?null:<TouchableOpacity style={{ flexDirection: "row" }} onPress={() => Linking.openURL(this.state.detail.manifest_print)}>
                      <SvgUri
							style={{ marginRight: 6, marginLeft: 30, color: "#fff" }}
							width="46"
              height="46"
							source={{ uri: 'https://www.hecmo.com/media/mobile/seller/My_Order_History.svg' }}
						/>
            <Text style={{ marginHorizontal: 10, color: "blue", marginTop: 10 }}>Print/Download</Text>
            </TouchableOpacity>}
            
            </View>
            </View>
                  </Card>
                </Content>

      </Container>
    );
  }
}

const mapStateToProps = (state) => {
	return {
	  token: state.HAReduce.token,
      user: state.HAReduce.user
	};
};
export default connect(mapStateToProps)(ProductDetails);
