import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Left, Icon, Body, Title, Button } from 'native-base';
import Cod from './Tabs/Cod';
import Prepaid from './Tabs/Prepaid';

export default class Transaction extends Component {
  render() {
    return (
      <Container>
                <Header style={{backgroundColor:'#48c7e8'}}>
                    <Left>
                    <StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor="#48c0e8"
          />
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Transactions</Title>
                    </Body>
                </Header>
         <Tabs>
          <Tab heading="COD" tabStyle={{ backgroundColor: '#fff' }} textStyle={{ color: '#000', fontWeight: 'normal' }} activeTabStyle={{ backgroundColor: '#f4f4f4', borderBottomWidth: 2 }} activeTextStyle={{ color: '#48c7e8', fontWeight: 'normal' }}>
            <Cod/>
          </Tab>
          <Tab heading="Prepaid" tabStyle={{ backgroundColor: '#fff' }} textStyle={{ color: '#000', fontWeight: 'normal' }} activeTabStyle={{ backgroundColor: '#f4f4f4', borderBottomWidth: 2 }} activeTextStyle={{ color: '#48c7e8', fontWeight: 'normal' }}>
            <Prepaid/>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}