import React, { Component } from 'react';
import { Platform, StatusBar } from 'react-native';
import { Container, Button, Header, Text, Content, Form, Item, Input, Left, Icon, Body, Title, Toast } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { DataCall } from '../utils/DataCall';
import styles from '../styles';
class Register extends Component {
	constructor() {
		super();
		this.state = {
			firstname: '',
			lastname: '',
			fullname: '',
			success_url: '',
			error_url: '',
			confirmation: '',
			email: '',
			company: '',
			mobile: '',
			password: '',
			nameValidate: false,
			passwordValidate: false,
			storeNameValidate: false,
			emailValidate: false,
			success: '',
			tempName: [],
			openStoreText : 'Open Your Store',
			submitForm: false,
			pleasewait : 'Please wait...',
		};
	}

	validate(text, type) {
		const alph = /^[a-zA-Z]+$/;
		const num = /^([a-zA-Z0-9@*#]{8,15})$/;
		const emails = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
		const mobiles = /^[0]?[789]\d{9}$/;
		const store = /^[a-zA-Z]+$/;
		if (type == 'firstname') {
			if (alph.test(text)) {
				this.setState({ nameValidate: true });
				this.setState({ firstname: text });
			} else {
				this.setState({ nameValidate: false });
			}
		} else if (type == 'lastname') {
			if (alph.test(text)) {
				this.setState({ nameValidate: true });
				this.setState({ lastname: text });
			} else {
				this.setState({ nameValidate: false });
			}
		} else if (type == 'fullname') {
			if (alph.test(text)) {
				this.setState({ nameValidate: true });
				this.setState({ fullname: text });
			} else {
				this.setState({ nameValidate: false });
			}
		} else if (type == 'email') {
			if (emails.test(text)) {
				this.setState({ emailValidate: true });
				this.setState({ email: text });
			} else {
				this.setState({ emailValidate: false });
			}
		} else if (type == 'mobile') {
			if (mobiles.test(text)) {
				this.setState({ mobileValidate: true });
				this.setState({ mobile: text });
			} else {
				this.setState({ mobileValidate: false });
			}
		} else if (type == 'password') {
			if (num.test(text)) {
				this.setState({ passwordValidate: true });
				this.setState({ password: text });
			} else {
				this.setState({ passwordValidate: false });
			}
		} else if (type == 'confirmation') {
			if (num.test(text)) {
				this.setState({ passwordValidate: true });
				this.setState({ confirmation: text });
			} else {
				this.setState({ passwordValidate: false });
			}
		} else if (type == 'company') {
			if (store.test(text)) {
				this.setState({ storeNameValidate: true });
				this.setState({ company: text });
			} else {
				this.setState({ storeNameValidate: false });
			}
		}
		if (type == 'success_url') {
			this.setState({ success_url: text });
		}
		if (type == 'error_url') {
			this.setState({ error_url: text });
		}
	}


	async updatingProfile() {
		if(this.state.submitForm) return;
		let profileData = new FormData();
		const fullNamess = this.state.fullname.split(' ');
		await this.setState({
			 firstname: fullNamess[0],
			 lastname: fullNamess[1],
			 submitForm: true,
		 });
		profileData.append('firstname', this.state.firstname);
		profileData.append('lastname', this.state.lastname);
		profileData.append('fullname', this.state.fullname);
		profileData.append('success_url', this.state.success_url);
		profileData.append('error_url', this.state.error_url);
		profileData.append('confirmation', this.state.confirmation);
		profileData.append('email', this.state.email);
		profileData.append('company', this.state.company);
		profileData.append('mobile', this.state.mobile);
		profileData.append('password', this.state.password);
		const ds = await DataCall.register(profileData, this.props.token);
		this.setState({
			submitForm: false,
			firstname: '',
			lastname: '',
			fullname: '',
			success_url: '',
			error_url: '',
			confirmation: '',
			email: '',
			company: '',
			mobile: '',
			password: '',
			nameValidate: false,
			passwordValidate: false,
			storeNameValidate: false,
			emailValidate: false,
		});
		if (ds.status === 1) {
			this.showToast('Registration successfully, Our team will confirm your account within 24 hrs.', 'success');
		} else {
			this.showToast(ds.message, 'danger');
		}
	}
	async validateUsername(text) { await this.setState({ fullname: text }); }
	showToast(str, ty) {
		this.setState({
			isChecking: false
		});
		Toast.show({
			text: str,
			buttonText: 'Close',
			type: ty,
			duration: 5000
		});
	}

	render() {
		return (
			<Container>
				<Header style={styles.header}>
				<StatusBar
					barStyle="light-content"
					hidden={false}
					backgroundColor="#48c0e8"
				/>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" />
						</Button>
					</Left>
					<Body>
						<Title>Register With Us</Title>
					</Body>
				</Header>

				<Content style={styles.contents}>
					<KeyboardAwareScrollView
						enableOnAndroid={true}
						style={{ height: '100%' }}
						enableAutoAutomaticScroll={Platform.OS === 'android'}
						extraHeight={130}
						extraScrollHeight={130}
					>
						<Form>
							
							<Item floatingLabel>
								<Input
									placeholder="Full Name"
									style={[ !this.state.nameValidate ? styles.error : null ]}
									onChangeText={(text) => this.validateUsername(text)}
								/>
							</Item>

							<Item floatingLabel>
								<Input
									placeholder="Email ID"
									onChangeText={(text) => this.validate(text, 'email')}
								/>
							</Item>
							<Item floatingLabel>
								<Input
									placeholder="Store name"
									onChangeText={(text) => this.validate(text, 'company')}
								/>
							</Item>

							<Item floatingLabel>
								<Input
									placeholder="Mobile No"
									maxLength={12}
									onChangeText={(text) => this.validate(text, 'mobile')}
								/>
							</Item>

							<Item floatingLabel>
								<Input
									placeholder="Password"
									secureTextEntry
									style={[ !this.state.passwordValidate ? styles.error : null ]}
									onChangeText={(text) => this.validate(text, 'password')}
								/>
							</Item>

							<Item floatingLabel>
								<Input
									placeholder="Confirm Password"
									secureTextEntry
									style={[ !this.state.passwordValidate ? styles.error : null ]}
									onChangeText={(text) => this.validate(text, 'confirmation')}
								/>
							</Item>
						</Form>

						<Button
							style={[ styles.loginbtn, {width:200,  marginTop: 20, marginLeft: 60 } ]}
							onPress={() => this.updatingProfile()}
							
						>
							<Text style={styles.logintxt}>{this.state.submitForm ? this.state.pleasewait : this.state.openStoreText}</Text>
						</Button>
						<Text style={[ styles.logindont, { marginBottom: 40, marginTop: 10 } ]}>
							Already Signup?
							<Text
								style={{
									color: '#48c0e8',
									marginLeft: 5
								}}
								onPress={() => this.props.navigation.navigate('Login')}
							> Login
							</Text>{' '}
						</Text>
					</KeyboardAwareScrollView>
				</Content>
			</Container>
		);
	}
}

const mapStateToProps = (state, oldprops) => {
	return {
		token: state.HAReduce.token,
		user: state.HAReduce.user
	};
};
export default connect(mapStateToProps)(Register);
