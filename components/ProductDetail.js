import React, {Component} from 'react';
import {
  Image,
  View,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Text,
  StatusBar,
  ActivityIndicator,
  Button
} from 'react-native';
// eslint-disable-next-line max-len
import {
  Container,
  Header,
  Content,
  Body,
  Title,
  Tab,
  Tabs,
  Icon,
  Right,
  Left,
  Form,
  Input,
  Item,
  Toast
} from 'native-base';
import axios from 'axios';
import { connect } from 'react-redux';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import {Dropdown} from 'react-native-material-dropdown';

const datas = [
  {value: '1', label: 'IN STOCK'},
  {value: '0', label: 'OUT OF STOCK'},
];

class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      stockValue: '',
      attrib: [],
      quantity: 0,
      price : 0,
      special_price: 0
    };
  }

  async componentDidMount() {
    const that = this;
    await axios
      .get(
        `https://www.hecmo.com/rest/V1/seller/product?product_id=${this.props.navigation.getParam('slug')}`,{
          headers: {
            Authorization: `Bearer ${this.props.token}`
        }
        })
      .then(response => {
        that.setState({dataSource: response.data});
      })
      .catch(error => {});
      const attt=this.state.dataSource;
      await this.setState({
        attrib: attt.attributes,
        quantity: attt.stock,
        price : attt.price,
        special_price: attt.special_price,
      });
  }
  
async updateProduct() {
  let that =this;
  await axios.post(`https://www.hecmo.com/rest/V1/seller/product/stock`, {
    product_id: this.props.navigation.getParam('slug'),
    stock_status: this.state.stockValue,
    quantity: this.state.quantity,
    price : this.state.price,
    special_price: this.state.special_price,
  }, {
    headers: {
      Authorization: `Bearer ${this.props.token}`
             }
    }).then(function(response){
      let product  = that.state.dataSource;
      product.price= that.state.price;
      product.special_price= that.state.special_price;
      product.check_stock= that.state.stockValue;
      that.setState({
        dataSource: product
      });
    });
    Toast.show({
			text: "Updated Succesfully",
			buttonText: 'Close',
			type: 'success',
			duration: 3000
		  });
} 

  updateStockAvailability(value) {
    this.setState({
      stockValue: value,
    });
  }

  render() {
    const detailsslug = this.props.navigation.getParam('slug');

    const product = this.state.dataSource;

    const atttribute = this.state.attrib;

    return (
      <Container>
        <Header style={{backgroundColor: '#48c7e8'}}>
          <StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor="#48c0e8"
          />
          <Left>
            <Icon
              name="arrow-back"
              style={{color: 'white'}}
              onPress={() => this.props.navigation.goBack()}
            />
          </Left>
          <Body>
            <Title>Detail Page</Title>
          </Body>
          <Right />
        </Header>
        <ScrollView>
          <Content style={{marginTop: 30}}>
            <TouchableHighlight
              onPress={() =>
                this.props.navigation.navigate('ImageZoomPage', {
                  slug: product.image_link,
                  dslug: detailsslug,
                })
              }>
              <Image
                source={{uri: product.image_link}}
                style={{height: 270, width: '100%'}}
              />
            </TouchableHighlight>
            <Content style={{fontSize: 40, marginTop: 20}}>
              <Text style={{fontSize: 18, textAlign: 'center'}}>
                {product.name}
              </Text>
            </Content>

            <Content
              style={{marginHorizontal: 10, fontSize: 40, marginTop: 20}}>
              <Text style={product.check_stock==0 ? {fontSize: 18, textAlign: 'center', color: 'red'}:{fontSize: 18, textAlign: 'center', color: 'green'}}>
                {product.check_stock==0 ? 'OUT OF STOCK':'IN STOCK'}
              </Text>
            </Content>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginTop: 10,
              }}>
                <Text
                style={(product.special_price=="0.00" || product.special_price==null || product.special_price=="0")?{fontSize: 16}:{fontSize: 14,
                  marginHorizontal: 10,
                  marginTop: 3,
                  textDecorationLine: 'line-through',
                  color: 'gray',
                }}>
                {' '}
                Rs {product.price}
              </Text>
              {product.special_price=="0.00"?null:<Text style={{fontSize: 16}}>Rs {product.special_price}</Text>}
            </View>

            <Text
              style={{
                width: '100%',
                borderBottomWidth: 2,
                borderBottomColor: '#f4f4f4',
  }}></Text>

<View style={{ minHeight: 100, marginHorizontal: 15, }}>
<Text style={{  fontWeight: "bold", paddingVertical: 10 }}>Description</Text>  
<Text style={{ width: '100%' }}>{product.short_description}</Text>
</View>

<Text style={{ marginLeft: 15, fontWeight: "bold", paddingVertical: 10 }}>Product Details</Text>

{Object.keys(atttribute).map((key, k) => (
    <View style={{ flexDirection: "row", marginLeft: 20, borderBottomWidth: 2,borderBottomColor: "#f4f4f4" }}>
    <Text style={{ color: "#000", }}>{atttribute[key].label}</Text>
    <Text style={{ color: "#000", marginLeft: "60%", position: "absolute" }}>:</Text>
    <Text style={{ color: "#000", marginLeft: "67%",position: "absolute" }}>{atttribute[key].value ? atttribute[key].value: '   -'}</Text>
    </View>
))}

<Text style = {{
                width: '100%',
                borderBottomWidth: 2,
                borderBottomColor: '#f4f4f4',
                marginTop: 10
              }}></Text>

            <Content style={{ marginHorizontal: 10 }}>
              <Dropdown
                data={datas}
                label="Product availability"
                value={product.check_stock}
                defaultValue={product.check_stock}
                onChangeText={(value, index, data) =>
                  this.updateStockAvailability(value, index, data)
                }
              />

            </Content>

            <Content style={{ marginTop: 10, color: "#f4f4f4", marginHorizontal: 10 }}>
              <Text style={{ color: "gray" }}>Quantity</Text>
                    
                            <Input
                            style ={{ borderWidth: 2, borderColor: "#f4f4f4" }}
                                placeholder="0"
                                value={this.state.quantity}
                                onChangeText={(text) => this.setState({ quantity: text })}
                            />
                        </Content>
                        <Content style={{ marginTop: 10, color: "#f4f4f4", marginHorizontal: 10 }}>
              <Text style={{ color: "gray" }}>Price</Text>
                    
                            <Input
                            style ={{ borderWidth: 2, borderColor: "#f4f4f4" }}
                                placeholder="0"
                                value={this.state.price}
                                onChangeText={(text) => this.setState({ price: text })}
                            />
                        </Content>
                        <Content style={{ marginTop: 10, color: "#f4f4f4", marginHorizontal: 10 }}>
              <Text style={{ color: "gray" }}>Special Price</Text>
                    
                            <Input
                            style ={{ borderWidth: 2, borderColor: "#f4f4f4" }}
                                placeholder="0"
                                value={parseInt(this.state.special_price)<parseInt(this.state.price) ? this.state.special_price : 0 }
                                onChangeText={(text) => this.setState({ special_price: text })}
                            />
                        </Content>
<View style={{ marginHorizontal: 110,marginTop: 30, marginBottom: 30 }}>
                        <Button title="Submit" color= "#48c7e8" onPress={() => this.updateProduct()} />
                        </View>     
                   </Content>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = (state, oldprops) => {
	return {
		token: state.HAReduce.token
	};
};

export default connect(mapStateToProps)(ProductDetail);
