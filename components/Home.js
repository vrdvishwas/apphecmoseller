import React, {Component} from 'react';
import { StyleSheet, View, ScrollView, StatusBar, Button, Image } from 'react-native';
import {
  Container,
  Header,
  Content,
  Card,
  Body,
  Text,
  Icon,
  Left,
  Title 
} from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import {connect} from 'react-redux';
import {DataCall} from '../utils/DataCall';
import SvgUri from 'react-native-svg-uri';
import { userData, tokenValue } from '../store/actions/TokenAction';
import {Grid, LineChart, XAxis, YAxis} from 'react-native-svg-charts';
import styles from '../styles';
const datas = [
  { value: '24h', label: 'Last 24 Hours' },
  { value: '7d', label: 'Last 7 Days' },
  { value: '1m', label: 'Current month' },
  { value: '1y', label: 'This year to date' },
  { value: '2y', label: 'Last year to date' },
];
const drawerImage = require('../assets/logo.png');

class Home extends Component {
  constructor() {
    super();
    this.state = {
      charts: {
        labels: ['no-data'],
        data: [],
      },
      isLoding: false,
      hm: '',
    };
  }

  async componentWillMount() {
    this.showData();
    this.chartData('2y', 4, datas);
  }

  async chartData(value) {

    let chartsData = await DataCall.chartApiData(value, this.props.token);
    this.setState({
      charts: chartsData
    });
  }

  async showData() {
    let home = await DataCall.homeData(this.props.token);
    this.setState({hm: home});
  }

  render() {
    let data = this.state.charts.data;
    let labels = this.state.charts.labels;
    const axesSvg = {fontSize: 11, fill: 'grey'};
    const verticalContentInset = {top: 10, bottom: 10};
    const xAxisHeight = 1;
    return (
      <Container>
        <Header style={{backgroundColor: '#fff'}}>
          <StatusBar
            barStyle="dark-content"
            hidden={false}
            backgroundColor="#fff"
          />
          <Left>
            <Icon
              style={{color: '#48c7e8'}}
              onPress={() => this.props.navigation.openDrawer()}
              name="menu"
            />
          </Left>

          <Body>
            <Image source={drawerImage} style={{ height: 35, width: 90 }} />
          </Body>
        </Header>

        <Content style={styles.contents}>
          <View style={styles.homeviews}>
            <Card style={styles.homecards}>

            <Text numberOfLines={1} style={styles.hometext}>
                Order to be processed
              </Text>

              <SvgUri
                style={styles.homesvgs}
                width="43"
                height="33"
                source={{
                  uri:
                    'https://www.hecmo.com/media/mobile/seller/Order_to_be_processed.svg',
                }}
              />
              
              <Button
                color="#48c7e8"
                onPress={() => this.props.navigation.navigate('OderHistory')}
                title="FULFILL ORDERS"
              />
              
              <View style={{ flexDirection: "row" }}>
              <Text style={styles.hometext}>
                New Order
              </Text>
              <Text style={{ fontSize: 12,position: "absolute", marginLeft: "50%", marginVertical: 5 }} >
                :
              </Text>
              <Text style={{ fontSize: 12,position: "absolute", marginLeft: "56%", marginVertical: 5 }}>
                {this.state.hm.new_order}
              </Text>  
              </View>

              <View style={{ flexDirection: "row" }}>
              <Text style={styles.hometext}>
              Upcoming Order
              </Text>
              <Text style={{ fontSize: 12,position: "absolute", marginLeft: "60%",marginVertical: 5 }} >
                :
              </Text>
              <Text  style={{ fontSize: 12,position: "absolute", marginLeft: "65%", marginVertical: 5 }}>
              {this.state.hm.order_to_approve}
              </Text>  
              </View>

            </Card>
            <Card style={styles.homecards}>
            <Text numberOfLines={1} style={styles.hometext}>
                Ready to dispatch
              </Text>
              <SvgUri
                style={styles.homesvgs}
                width="43"
                height="33"
                source={{
                  uri:
                    'https://www.hecmo.com/media/mobile/seller/Ready_to_dispatch.svg',
                }}
              />

              <Button
                color="#48c7e8"
                onPress={() => this.props.navigation.navigate('OderHistory')}
                title="DISPATCH MANIFEST"
              />
              <Text numberOfLines={1} style={styles.hometext}>
                Ready to Dispatch  : {this.state.hm.ready_dispatch}
              </Text>
            </Card>
          </View>
          <View style={styles.homeviews}>
            <Card style={styles.homecards}>
            <Text style={styles.hometext}>Return/Cancel</Text>
              <SvgUri
                style={styles.homesvgs}
                width="43"
                height="33"
                source={{
                  uri:
                    'https://www.hecmo.com/media/mobile/seller/Return_Cancel.svg',
                }}
              />
              
              <Button 
              color="#48c7e8" 
              title="VIEW RETURNS"
              onPress={() => this.props.navigation.navigate('RMAPage')} />

              <Text style={styles.hometext}>
                Return Request(s) : {this.state.hm.rma_return}
              </Text>
              <Text numberOfLines={1} style={styles.hometext}>
                Cancel Request(s) : {this.state.hm.rma_cancel} </Text>
            </Card>
            <Card style={styles.homecards}>
            <Text style={styles.hometext}>Due Payments</Text>
              <SvgUri
                style={styles.homesvgs}
                width="43"
                height="33"
                source={{
                  uri: 'https://www.hecmo.com/media/mobile/seller/payment.svg',
                }}
              />

              <Button color="#48c7e8" title="DUE PAYMENTS" onPress={() => this.props.navigation.navigate('Transaction')}/>
              <Text style={styles.hometext}>
                Amount: {this.state.hm.Prepaid}
              </Text>
            </Card>
          </View>

          <Card>
            <Text
              style={{
                color: '#48c7e8',
                marginVertical: 10,
                textAlign: 'center',
              }}>
              YOUR INCOME
            </Text>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <Card style={styles.homeincard}>
                <Text numberOfLines={1} style={{fontSize: 12, marginBottom: 2}}>
                  TODAY
                </Text>
                <Icon
                  name="calendar"
                  style={{color: '#48c7e8', fontSize: 36}}
                />
                <Text style={styles.homeprice}>{this.state.hm.today_sale}</Text>
              </Card>
              <Card style={styles.homeincard}>
                <Text numberOfLines={1} style={{fontSize: 12, marginBottom: 2}}>
                  WEEK
                </Text>
                <Icon
                  name="calendar"
                  style={{color: '#48c7e8', fontSize: 36}}
                />
                <Text style={styles.homeprice}>{this.state.hm.week_sale}</Text>
              </Card>
              <Card style={styles.homeincard}>
                <Text numberOfLines={1} style={{fontSize: 12, marginBottom: 2}}>
                  MONTH
                </Text>
                <Icon
                  name="calendar"
                  style={{color: '#48c7e8', fontSize: 36}}
                />
                <Text style={styles.homeprice}>{this.state.hm.month_sale}</Text>
              </Card>
            </View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <Card style={styles.homeincard}>
              <SvgUri
                style={styles.homesvgs}
                width="43"
                height="33"
                source={{
                  uri: 'https://www.hecmo.com/media/mobile/seller/payment.svg',
                }}
              />
                <Text numberOfLines={1} style={styles.homeprice}>
                  TOTAL-
                </Text>
                <Text style={styles.homeprice}>{this.state.hm.total_sale}</Text>
              </Card>

              <Card style={styles.homeincard}>
              <SvgUri
                style={styles.homesvgs}
                width="43"
                height="33"
                source={{
                  uri: 'https://www.hecmo.com/media/mobile/seller/payment.svg',
                }}
              />
                <Text numberOfLines={1} style={styles.homeprice}>
                  DUE PAYMENTS-
                </Text>

                <Text style={styles.homeprice}>
                  {this.state.hm.remain_amount}
                </Text>
              </Card>
            </View>
          </Card>

          <Text
              style={{
                color: '#48c7e8',
                marginVertical: 10,
                textAlign: 'center',
              }}>
              SALES CHART
            </Text>
                   
          <Dropdown
            data={datas}
            label="Duration"
            value="2y"
            onChangeText={(value, index, data) =>
              this.chartData(value, index, data)}/>
          <ScrollView horizontal={true}>
            <View style={{ height: 300, padding: 10, flexDirection: 'row', marginBottom: 20 }}>
              <YAxis
                data={data}
                style={{marginBottom: xAxisHeight}}
                contentInset={verticalContentInset}
                svg={axesSvg}
              />
              <View style={{flex: 1, marginLeft: 0}}>
                <LineChart
                  style={{flex: 1}}
                  data={data}
                  contentInset={verticalContentInset}
                  svg={{stroke: '#48c7e8'}}>
                  <Grid />
                </LineChart>
                <XAxis
                  style={{
                    marginHorizontal: -10,
                    height: xAxisHeight,
                    width: 800,
                  }}
                  data={data}
                  formatLabel={(value, index) => labels[index]}
                  contentInset={{ left: 30, right: 30 }}
                  svg={axesSvg}
                  spacingInner={50}
                  numberOfTicks={13}
                />
              </View>
            </View>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styless = StyleSheet.create({
  lgtxt: {marginLeft: 14},
});

const mapStateToProps = state => {
  return {
    token: state.HAReduce.token,
    user: state.HAReduce.user,
  };
};
export default connect(mapStateToProps)(Home);
