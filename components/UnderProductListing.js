import React from 'react';
import {
  Image,
  Platform,
  View,
  Text,
  TouchableWithoutFeedback,
} from 'react-native';
import {Container, Card, Icon} from 'native-base';

import styles from '../styles';

const isIOS = Platform.OS === 'android';

export class UnderProductListing extends React.Component {
  shouldComponentUpdate(newProps) {
    return this.props.product !== newProps.product;
  }

  componentWillUpdate() {
    //On iOS while recycling till the new image is loaded the old one remains visible. This forcefully hides the old image.
    //It is then made visible onLoad
    if (isIOS && this.imageRef) {
      this.imageRef.setNativeProps({
        opacity: 0,
      });
    }
  }

  handleOnLoad = () => {
    if (isIOS && this.imageRef) {
      this.imageRef.setNativeProps({
        opacity: 1,
      });
    }
  };

  render() {
    let item = this.props.product;
    let token = this.props.nav.token;
    let that = this;
    let dotColor = this.props.product.visibility!=1 && this.props.product.status==1 ? 'green' : 'red';
    return (
      <Container style={{width: '100%'}}>
        <TouchableWithoutFeedback
          onPress={() =>
            this.props.nav.navigation.navigate('ProductDetail', {slug: item.id})
          }>
          <Card style={{flex: 1}}>
            <View style={{ flexDirection: "row" }}>
            <Text style={{ height: 10, width: 10, backgroundColor: dotColor, marginLeft:5,marginTop: 5, borderRadius: 5,borderBottomColor: dotColor }}></Text>
            <Icon name="create" style={{ color: "#48c7e8",position: 'absolute', right: 10 }} />
            </View>
            
            <View style={{flexDirection: 'row'}}>
              <View style={{width: '30%'}}>
                <Image
                  ref={ref => {
                    that.imageRef = ref;
                  }}
                  onLoad={that.handleOnLoad}
                  style={{height: 110, width: 110, marginTop: 10}}
                  source={{
                    uri: item.image_link,
                  }}
                />
              </View>

              <View>
                <Text
                  numberOfLines={3}
                  style={{
                    width: '65%',
                    marginTop: 10,
                    marginRight: 10,
                    marginLeft: 15,
                    fontWeight: 'bold'
                  }}>
                  {item.title}
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 10,
                    marginLeft: '4%',
                  }}>

                  <View style={{ flexDirection: "row" }}>
                  <Text
                  style={{
                      fontWeight: 'bold',
                      fontSize: 15
                      }}>Rs</Text>
                  <Text 
                  style={{ 
                    marginLeft: 3,
                    fontWeight: 'bold'
                         }}>{item.offer_price}</Text>
                  </View>
  
                  {item.offer_price==item.price?null:
                  <Text
                  style={{
                    textDecorationLine: 'line-through',
                    color: 'gray',
                    marginLeft:5
                  }}>
                    Rs {item.price}
                  </Text>}
                  
                  <Text
                    style={{
                      color: '#48c7e8',
                      // textAlign: 'center',
                      // justifyContent: 'center',
                      marginLeft: '4%'
                    }}>
                    {item.price != item.offer_price
                      ? `${Math.round(
                          100 - (item.offer_price * 100) / item.price,
                        )}%`
                      : null}
                  </Text>

                </View>

                <View
                  style={{
                    marginTop: 10,
                    marginLeft: 15,
                  }}>
                  <Text>Brand: {item.brand}</Text>
                </View>

                <View>
                  <Text style={{marginLeft: 15, marginTop: 10}}>
                    {item.availability == 1 ? 'IN STOCK' : 'OUT OF STOCK'}
                  </Text>
                </View>
 
                {/* <View>
                <Text style={{ marginTop: 10, marginLeft: 15 }}>
              {item.datetime}
            </Text>
                </View> */}
              </View>
            </View>
          </Card>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
}
