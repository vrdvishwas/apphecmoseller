import React from 'react';
import {Image, Platform, View} from 'react-native';
import {Container, Card, Button, Text} from 'native-base';
import styles from '../styles';
// import {TouchableWithoutFeedback} from 'react-native-gesture-handler';

export class UnderOrderHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: '',
    };
  }
  shouldComponentUpdate(newProps) {
    return this.props.product !== newProps.product;
  }

  handleOnLoad = () => {
    if (isIOS && this.imageRef) {
      this.imageRef.setNativeProps({
        opacity: 1,
      });
    }
  };

  render() {
    let item = this.props.product;
    return (
      <Container>
        {/* <TouchableWithoutFeedback
          onPress={() =>
            this.props.nav.navigation.navigate('OrderDetails', {
              slug: item.orderid,
            })
          }> */}
          <Card style={{marginHorizontal: 15, padding: 10, height: 200}}>
            <Text style={{color: '#48c7e8', marginLeft: 10, fontWeight: 'bold' }}>
              #{item.order_id}
            </Text>
            <Text
              numberOfLines={1}
              style={{marginVertical: 10 }}>
              {item.items}
            </Text>
            <Text style={{padding: 2}}>Name: {item.customer_name}</Text>
            <Text style={{padding: 2}}>Date: {item.order_date_time}</Text>
            <Text style={{padding: 2}}>Amount: Rs {item.order_amount}</Text>
            <View style={{flexDirection: 'row'}}>
              {item.is_approved=="1"?<Text
                style={{padding: 5, marginTop: 10, color: '#48c7e8' }}
                onPress={() =>
                  this.props.nav.navigation.navigate('OrderDetails', {
                    slug: item.orderid,
                  })
                }>View Order</Text>:null}
              <Text
                style={[
                  {position: 'absolute', right: 20, marginTop: 15, fontWeight: 'bold'},
                  styles[item.status],
                ]}>
                {item.status_label.toUpperCase()}
              </Text>
            </View>
          </Card>
        {/* </TouchableWithoutFeedback> */}
      </Container>
    );
  }
}
