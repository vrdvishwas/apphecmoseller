/* eslint-disable react/jsx-no-undef */
import React, { Component } from 'react';
import { Image, View, TouchableHighlight, StyleSheet } from 'react-native';
// eslint-disable-next-line max-len
import { Container, Header, Content, Left, Body, Text, Title, Button, Tab, Tabs, Icon, Item ,Card, Form, Input} from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';


export default class OderHistory extends Component {
  render() {

    let data = [ 
        {value: '9',
    }, {
      value: '15',
    },
    {value:'30'},
    {value:'all'},
    ];
    let items = [ 
        {value: '9',
    }, {
      value: '15',
    },
    {value:'30'},
    {value:'all'},
    ];

    return (
      <Container>
         <Header
                 style={{
                      backgroundColor: '#48c7e8',
                      textAlign: 'center',
                      justifyContent: 'center',
                    }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>My Requested RMA</Title>
                    </Body>
                </Header>
        <Content>
        <View style={{flex:1,flexDirection:'row',height:100,marginTop:20}}>
            <Text>2 item(s)</Text>
            <Text> SHOW: </Text>
            <Dropdown
               data={items}
               containerStyle={{
               width: "40%",position:'relative',left:10,bottom:33}}/>
          </View>

        <Card style={styles.carddeta}>
   
            <Text style={styles.text}>ID</Text>
            <Text style={styles.nortxt}>133</Text>
            <Text style={styles.text}>CUSTOMER NAME</Text>
            <Text style={styles.nortxt}>Vishal Chanana</Text>
            <Text style={styles.text}>ORDER ID</Text>
            <Text style={styles.nortxt}>100000352</Text>
            <Text style={styles.text}>PRODUCT NAME</Text>
            <Text numberOfLines={2} style={styles.nortxt}>St. Ives Imported Even And Bright, Pink Lemon & Mandarin Body Wash (400ml-Pack of 2)</Text>
            <Text style={styles.text}>REASON</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Shipping cost too high</Text>
            <Text style={styles.text}>DATE</Text>
            <Text numberOfLines={1} style={styles.nortxt}>12/Jul/2018 12:03:12 pm</Text>
            <Text style={styles.text}>RMA STATUS</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Solved</Text>
            <Text style={styles.text}>MY STATUS</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Pending</Text>
            <Text style={styles.text}>CUSTOMER STATUS</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Delivered</Text>
            <Text style={styles.vieworder} onPress={() => this.props.navigation.navigate('ReturnView')}>View</Text>
        </Card>
   
        <Card style={styles.carddeta}>
        <Text style={styles.text}>ID</Text>
            <Text style={styles.nortxt}>72</Text>
            <Text style={styles.text}>CUSTOMER NAME</Text>
            <Text style={styles.nortxt}>Vishal Chanana</Text>
            <Text style={styles.text}>ORDER ID</Text>
            <Text style={styles.nortxt}>100000146</Text>
            <Text style={styles.text}>PRODUCT NAME</Text>
            <Text numberOfLines={2} style={styles.nortxt}>St. Ives Imported Even And Bright, Pink Lemon & Mandarin Body Wash (400ml-Pack of 2)</Text>
            <Text style={styles.text}>REASON</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Item would not arrive on time</Text>
            <Text style={styles.text}>DATE</Text>
            <Text numberOfLines={1} style={styles.nortxt}>27/May/2018 4:53:17 pm</Text>
            <Text style={styles.text}>RMA STATUS</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Solved</Text>
            <Text style={styles.text}>MY STATUS</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Pending</Text>
            <Text style={styles.text}>CUSTOMER STATUS</Text>
            <Text numberOfLines={1} style={styles.nortxt}>Delivered</Text>
            <Text style={styles.vieworder} onPress={() => this.props.navigation.navigate('ReturnView')}>View</Text>
        </Card>
        


          <View style={{flex:1,flexDirection:'row',height:100,marginTop:20}}>
            <Text>2 item(s)</Text>
            <Text> SHOW: </Text>
            <Dropdown
               data={items}
               containerStyle={{
               width: "40%",position:'relative',left:10,bottom:33}}/>
          </View>
        </Content>
      </Container>
    );
  }
}
const styles=StyleSheet.create({
  nortxt:{marginLeft:5},
  text:{fontWeight:'bold',marginLeft:5},
  vieworder:{color:'blue',marginLeft:4,fontSize:18,marginTop:3,marginBottom:5},
  carddeta:{width:348,marginTop:10,marginLeft:5,marginRight:10},
  btn:{width:142,height:20,marginLeft:2,marginTop:5,marginRight:70,},
  btntext:{fontSize:12,textAlign:'center'},

});