import React, { Component } from 'react';
import { Platform, StatusBar, ActivityIndicator, Alert, View, Modal, Linking } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import {Right, Card, Header, Container, Content, Body, Left, Button, Title, Icon, Form, Item, Input, Text, Label, Toast} from 'native-base';
import { TextInput, TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import SvgUri from 'react-native-svg-uri';
// import Modal from "react-native-modal";
import {userData, tokenValue} from '../store/actions/TokenAction';
import {DataCall} from '../utils/DataCall';
import styles from '../styles';

class Shipping extends Component {
constructor(props){
  super(props);
  this.state = {
      isVerify: 0,
      mode:false,
      shippping_pickup_address:'',
      shippping_pickup_city:'',
      shippping_pickup_state:'',
      shippping_pickup_pincode:'',
      shippping_pickup_contactname:'',
      shippping_pickup_contanctno:'',
      shippping_price_500: '',
      shppping_price_1000:'',
      shippping_price_plus: '',
      fixed_shipping_amount:'',
      free_shipping_above_order: '',
      free_shipping_skus: '',
      shipping_skus: '',
      is_free_shipping: '',
      is_fixed_shipping: '',
      TextInputDisableStatus: true,
      loader:true,
      showPopup: false
  }
}

async updatingProfile(){
  let profileData = new FormData();
if(this.state.shippping_pickup_address!="") {
  profileData.append("shippping_pickup_address", this.state.shippping_pickup_address);
}

if(this.state.shippping_pickup_city!="") {
  profileData.append("shippping_pickup_city", this.state.shippping_pickup_city);
}

if(this.state.shippping_pickup_state!="") {
  profileData.append("shippping_pickup_state", this.state.shippping_pickup_state);
}

if(this.state.shippping_pickup_pincode!="") {
  profileData.append("shippping_pickup_pincode", this.state.shippping_pickup_pincode);
}

if(this.state.shippping_pickup_contactname!="") {
  profileData.append("shippping_pickup_contactname", this.state.shippping_pickup_contactname);
}

if(this.state.shippping_pickup_contanctno!="") {
  profileData.append("shippping_pickup_contanctno", this.state.shippping_pickup_contanctno);
}

if(this.state.is_free_shipping!="") {
  profileData.append("is_free_shipping", this.state.is_free_shipping);
}

if(this.state.is_fixed_shipping!="") {
  profileData.append("is_fixed_shipping", this.state.is_fixed_shipping);
}

if(this.state.fixed_shipping_amount!="") {
  profileData.append("fixed_shipping_amount", this.state.fixed_shipping_amount);
}

if(this.state.free_shipping_above_order!="") {
  profileData.append("free_shipping_above_order", this.state.free_shipping_above_order);
}

if(this.state.free_shipping_skus!="") {
  profileData.append("free_shipping_skus", this.state.free_shipping_skus);
}

if(this.state.shipping_skus!="") {
  profileData.append("shipping_skus", this.state.shipping_skus);
}

if(this.state.shippping_price_500!="") {
  profileData.append("shippping_price_500", this.state.shippping_price_500);
}

if(this.state.shppping_price_1000!="") {
  profileData.append("shppping_price_1000", this.state.shppping_price_1000);
}

if(this.state.shippping_price_plus!="") {
  profileData.append("shippping_price_plus", this.state.shippping_price_plus);
}

  const ds=await DataCall.updateProfile(profileData, this.props.token);
// console.log("dsdsdsdsdsdsdsds", ds);
  Toast.show({
    text: ds.message,
    buttonText: 'Close',
    type: 'success',
    duration: 3000
    });
    const rs = await DataCall.profileData(this.props.token);
    await this.props.updateUserData(rs);
    this.props.navigation.goBack();    
  }
      async updateState(text, type) {
        if(type== 'shippping_pickup_address'){
          this.setState({shippping_pickup_address: text});
        }
        if(type== 'shippping_pickup_city'){
          this.setState({shippping_pickup_city: text});
        }
        if(type== 'shippping_pickup_state'){
          this.setState({shippping_pickup_state: text});
        }
        if(type== 'shippping_pickup_pincode'){
          this.setState({shippping_pickup_pincode: text});
        }
        if(type== 'shippping_pickup_contactname'){
          this.setState({shippping_pickup_contactname: text});
        }
        if(type== 'shippping_pickup_contanctno'){
          this.setState({shippping_pickup_contanctno: text});
        }
        // fixed_shipping_amount
        if(type== 'fixed_shipping_amount'){
          this.setState({fixed_shipping_amount: text});
        }
        // free_shipping_above_order
        if(type== 'free_shipping_above_order'){
          this.setState({free_shipping_above_order: text});
        }
        // free_shipping_skus
        if(type== 'free_shipping_skus'){
          this.setState({free_shipping_skus: text});
        }
        // shipping_skus
        if(type== 'shipping_skus'){
          this.setState({shipping_skus: text});
        }

        if(type== 'shippping_price_500'){
          this.setState({shippping_price_500: text});
        }
        if(type== 'shppping_price_1000'){
          this.setState({shppping_price_1000: text});
        }
        if(type== 'shippping_price_plus'){
          this.setState({shippping_price_plus: text});
        }
        }


        edit(mode) {

          setTimeout(()=> {
            this.setState({
              loader: false
            });
          }, 0);

          if (mode == true) {
            this.setState({ isVerify: 1 });
            this.setState({ TextInputDisableStatus: false });
          } else {
            this.setState({ isVerify: 0 });
          }
            }  

generatePopUp() {
this.setState({ showPopup: true})
}

updateIsFixedShipping(value) {
this.setState({
is_fixed_shipping: value
});
}

updateIsFreeShipping(value) {
  this.setState({
    is_free_shipping: value
    });
}

render() {
    let data = [
      {value:'1',label: 'Yes'},
      {value:'0',label: 'No'},
    ];
    
    let phoneNumber = 1800313000022;
    return ( 
   <Container>
   <Header style={styles.header}>
   <StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor="#48c0e8"
          />
        <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
                <Icon name="arrow-back" />
            </Button>
        </Left>
        <Body>
            <Title>Shipping Information</Title>
        </Body>
        {this.props.user.is_correct_info==="0" ? <Right>
						<Icon name="create" style={{ color: "white" }} onPress={() => this.edit(true)} />
					</Right> : <Right><TouchableOpacity onPress={() => this.generatePopUp()}><SvgUri
                style={styles.homesvgs}
                width="30"
                height="30"
                source={{ uri: 'https://www.hecmo.com/media/mobile/seller/lock.svg' }}
              /></TouchableOpacity></Right>}
      </Header>

      <Content>
      <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showPopup}
          onRequestClose={() => {
            this.setState({ showPopup: false });
          }}>
          <View style={styles.modalPopUp}>
            <Text style={{ right:10, fontSize: 28, color: "#fff", position: "absolute" }} onPress={()=> this.setState({ showPopup: false })}>x</Text>
              <Text style={{ color: "#fff", marginTop: 30 }}>Call at: </Text>
              <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 22,marginVertical: 3 }} onPress={()=> Linking.openURL(`tel:${phoneNumber}`) }>1800 313 000 022</Text>
              <Text style={{ color: "#fff",marginVertical: 5 }}>OR </Text>
              <Text style={{ color: "#fff",marginVertical: 3 }}>Email at:</Text>
              <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 22,marginVertical: 3 }} onPress={()=> Linking.openURL('mailto:care@hecmo.com') }>care@hecmo.com</Text>
          </View>
        </Modal>

        {this.state.isVerify==0 ? 
             
          <Content style={styles.contents}>
<Form>
              <Item stackedLabel>
              <Label>Pickup/Dispatch Address</Label>
                <Input value={this.props.user.shippping_pickup_address} editable={false} />
              </Item>
              <Item stackedLabel>
              <Label>City</Label>
                <Input value={this.props.user.shippping_pickup_city} editable={false}/>
              </Item>
              <Item stackedLabel>
              <Label>State</Label>
                <Input value={this.props.user.shippping_pickup_state} editable={false}/>
              </Item>
              <Item stackedLabel>
              <Label>Pincode</Label>
                <Input value={this.props.user.shippping_pickup_pincode} editable={false}/>
              </Item>
              <Item stackedLabel>
              <Label>Contact Name</Label>
                <Input value={this.props.user.shippping_pickup_contactname} editable={false}/>
              </Item>
              <Item stackedLabel>
              <Label>Contact No</Label>
                <Input value={this.props.user.shippping_pickup_contanctno} editable={false}/>
              </Item>
      
              <Item stackedLabel>
              <Label>Free Shipping on complete store</Label>
                <Input value={this.props.user.is_free_shipping==0? 'No':'Yes'} editable={false} />
              </Item>

              <Item stackedLabel>
              <Label>Is Fixed Shipping</Label>
                <Input value={this.props.user.is_fixed_shipping==0?'No':'Yes'} editable={false} />
              </Item>
              
              <Item stackedLabel>
              <Label>Fixed Shipping amount</Label>
                <Input value={this.props.user.fixed_shipping_amount} editable={false} />
              </Item>

              <Item stackedLabel>
              <Label>Free Shipping on above Order Total	</Label>
                <Input value={this.props.user.free_shipping_above_order} editable={false} />
              </Item>

              <Item stackedLabel>
              <Label>Free Shipping Sku</Label>
                <Input value={this.props.user.free_shipping_skus} editable={false} />
              </Item>

              <Item stackedLabel>
              <Label>Non Free Shipping Sku	</Label>
                <Input value={this.props.user.shipping_skus} editable={false} />
              </Item>

              <Item stackedLabel>
              <Label>Shipping Price upto 500 GM</Label>
                <Input value={this.props.user.shippping_price_500} editable={false} />
              </Item>
              <Item stackedLabel>
              <Label>Shipping Price upto 1000 GM</Label>
                <Input value={this.props.user.shippping_price_1000} editable={false}/>
              </Item>
              <Item stackedLabel>
              <Label>Shipping Price Above 1000/500 GM</Label>
                <Input value={this.props.user.shippping_price_plus} editable={false}/>
              </Item>
    </Form>
    </Content> :<Content style={styles.contents}>
<KeyboardAwareScrollView enableOnAndroid={true} style={{height:"100%"}}
            enableAutoAutomaticScroll={(Platform.OS === 'android')} extraHeight={130} extraScrollHeight={130}>
              <ActivityIndicator
              animating={this.state.loader}
        style={{ margin: 10, flex: 1 }}
        size="large"
        color={'black'}
      />
<Form>
<Item stackedLabel>
              <Label>Pickup/Dispatch Address</Label>
              <Input onChangeText={text => this.updateState(text, 'shippping_pickup_address')} >{this.props.user.shippping_pickup_address}</Input>
              </Item>
              <Item stackedLabel>
              <Label>City</Label>
                <Input onChangeText={text => this.updateState(text, 'shippping_pickup_city')}>{this.props.user.shippping_pickup_city}</Input>
              </Item>
              <Item stackedLabel>
              <Label>State</Label>
                <Input onChangeText={text => this.updateState(text, 'shippping_pickup_state')}>{this.props.user.shippping_pickup_state}</Input>
              </Item>
              <Item stackedLabel>
              <Label>Pincode</Label>
                <Input onChangeText={text => this.updateState(text, 'shippping_pickup_pincode')}>{this.props.user.shippping_pickup_pincode}</Input>
              </Item>
              <Item stackedLabel>
              <Label>Contact Name</Label>
                <Input onChangeText={text => this.updateState(text, 'shippping_pickup_contactname')}>{this.props.user.shippping_pickup_contactname}</Input>
              </Item>
              <Item stackedLabel>
              <Label>Contact No</Label>
                <Input onChangeText={text => this.updateState(text, 'shippping_pickup_contanctno')} >{this.props.user.shippping_pickup_contanctno}</Input>
              </Item>

              <Dropdown 
              label='Free Shipping on complete store'
              data={data}
              value={this.props.user.is_free_shipping} 
              onChangeText={(value, index, data) =>
                this.updateIsFreeShipping(value, index, data)
              }
              containerStyle={{
               width: "95%",
               marginLeft: 17,
               marginRight: 17}}/>

              <Dropdown 
              label='Is Fixed Shipping'
              value={this.props.user.is_fixed_shipping}
              onChangeText={(value, index, data) =>
                this.updateIsFixedShipping(value, index, data)
              }
              data={data} 
              containerStyle={{
               width: "95%",
               marginLeft: 17,
               marginRight: 17}}/>

              <Item stackedLabel>
              <Label>Fixed Shipping amount</Label>
                <Input onChangeText={text => this.updateState(text, 'fixed_shipping_amount')} >{this.props.user.fixed_shipping_amount}</Input>
              </Item>

              <Item stackedLabel>
              <Label>Free Shipping on above Order Total	</Label>
                <Input onChangeText={text => this.updateState(text, 'free_shipping_above_order')} >{this.props.user.free_shipping_above_order}</Input>
              </Item>

              <Item stackedLabel>
              <Label>Free Shipping Sku</Label>
                <Input onChangeText={text => this.updateState(text, 'free_shipping_skus')} >{this.props.user.free_shipping_skus}</Input>
              </Item>

              <Item stackedLabel>
              <Label>Non Free Shipping Sku	</Label>
                <Input onChangeText={text => this.updateState(text, 'shipping_skus')} >{this.props.user.shipping_skus}</Input>
              </Item>

              <Item stackedLabel>
              <Label>Shipping Price upto 500 GM</Label>
                <Input onChangeText={text => this.updateState(text, 'shippping_price_500')} >{this.props.user.shippping_price_500}</Input>
              </Item>

              <Item stackedLabel>
              <Label>Shipping Price upto 1000 GM</Label>
                <Input onChangeText={text => this.updateState(text, 'shippping_price_1000')} >{this.props.user.shippping_price_1000}</Input>
              </Item>
              <Item stackedLabel>
              <Label>Shipping Price Above 1000/500 GM</Label>
                <Input onChangeText={text => this.updateState(text, 'shippping_price_plus')} >{this.props.user.shippping_price_plus}</Input>
              </Item>
    </Form>

    <Button info style={styles.shippingbtn} onPress={() => this.updatingProfile()}><Text style={styles.savetxt}>SAVE</Text></Button>
</KeyboardAwareScrollView>
             </Content>
              }
              </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state, oldprops) => {

  return {
    token: state.HAReduce.token,
    user: state.HAReduce.user
  };
};

const mapDispatchToProps = dispatch => ({
  updateUserData: data => {
    dispatch(userData(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Shipping);