import React, { Component } from 'react';
import { View, Button, StatusBar, Text } from 'react-native';
import {
    Form,
    Item,
    Input,
    Header,
    Left,
    Icon,
    Body,
    Title
} from 'native-base';

import styles from '../styles';
import { DataCall } from '../utils/DataCall';
import { connect } from 'react-redux';

class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            oldPassword: '',
            passwordEntered: '',
            passwordEntered2: '',
            clickEvent: false
        };
    }
    async updateState(password, type) {
        { type === 'opswd' ? await this.setState({ oldPassword: password }) : null; }
        { type === 'pswd' ? await this.setState({ passwordEntered: password }) : null; }
        { type === 'rpswd' ? await this.setState({ passwordEntered2: password }) : null; }
    }

    sendChangedPassword() {
if (this.state.passwordEntered === this.state.passwordEntered2) {
const sendPassword = DataCall.changePassword(this.state.oldPassword, this.state.passwordEntered, this.state.passwordEntered2, this.props.token);
{ this.state.sendPassword === 401 ? alert('Something went wrong') : alert('Password changed successfully'); }
} else {
    alert('Entered passwords do not match');
}
    }

    render() {
        return (
            <View>
        <StatusBar barStyle="dark-content" hidden={false} backgroundColor="#00BCD4" translucent={true} />
                <Header
                    style={{
                        backgroundColor: '#48c7e8',
                        textAlign: 'center',
                        justifyContent: 'center',
                    }}
                >
                    <Left>

                        <Icon style={{color:"#fff"}} onPress={() => this.props.navigation.goBack()} name="arrow-back" />

                    </Left>
                    <Body>
                        <Title>Change Password</Title>
                    </Body>
                </Header>

                <View style={{ marginHorizontal: '3%', }}>
                    <Text style={{ marginTop: '10%' }}>Please enter your old and new passwords.</Text>
                </View>
                <Form style={styles.formEmail}>

                <Item floatingLabel>
                        <Input
                            placeholder="Enter Old Password"
                            secureTextEntry
                            onChangeText={(text) => this.updateState(text, 'opswd')}
                        />
                    </Item>

                    <Item floatingLabel>
                        <Input
                            placeholder="Enter new Password"
                            secureTextEntry
                            onChangeText={(text) => this.updateState(text, 'pswd')}
                        />
                    </Item>

                    <Item floatingLabel>
                        <Input
                        secureTextEntry
                            placeholder="Re-enter new Password"
                            onChangeText={(text) => this.updateState(text, 'rpswd')}
                        />
                    </Item>

                </Form>
                <View style={styles.loginbtn}>
                    <Button color="#48c7e8" title="Submit" disabled={this.state.clickEvent} onPress={() => this.sendChangedPassword()} />
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.HAReduce.token,
        user: state.HAReduce.user
      };
  };
  
export default connect(mapStateToProps)(ChangePassword);