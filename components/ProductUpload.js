import React,{Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, TouchableHighlight,} from 'react-native';
import {Card, Header, Container, Content, Body, Left, Button, Title, Icon,Col,Row} from 'native-base';


export default class ProductUpload extends Component{
  constructor(props){
    super(props)
    this.state ={
      selected:false,
      cardss : [
        {
         isSelected: false,
          name: 'BEAUTY'
        },
        {
          isSelected: false,
          name: 'JEWELERY'
         },
         {
          isSelected: false,
          name: 'BAG &WALLETS'
         },
         {
          isSelected: false,
          name: 'HAIR ACCESSORIES'
         },
         {
          isSelected: false,
          name: 'FRAGRANCE'
         },
         {
          isSelected: false,
          name: 'MOTHER & CHILD'
         },
         {
          isSelected: false,
          name: 'MENS WORLD',
         },
         {
          isSelected: false,
          name: 'HEALTH & WELLBEING'
         },
         {
          isSelected: false,
          name: 'FOOTER'
         },
         {
          isSelected: false,
          name: 'LINGERIE & SLEEPWEAR'
         },
         {
          isSelected: false,
           name: 'WOMEN ETHNIC WEAR'
         },
         {
          isSelected: false,
           name: 'WOMEN WESTERN WEAR'
         },
         
         
      ]
    }
    this.handleClick = this.handleClick.bind(this);
  }

  chunkArray(myArray, chunkSize) {
    let index = 0;
    const arrayLength = myArray.length;
    const tempArray = [];
    // eslint-disable-next-line camelcase
    for (index = 0; index < arrayLength; index += chunkSize) {
      const myChunk = myArray.slice(index, index + chunkSize);
      tempArray.push(myChunk);
    }
    return tempArray;
  }
  
  handleClick(key){
      let oldCards = this.state.cardss; 
      oldCards[key]['isSelected'] = !oldCards[key]['isSelected'];
      this.setState({
        cardss : oldCards
      })
  }
  render(){
    let that =  this;
    return(

              <Container>
                  <Header style={{ backgroundColor: '#48c7e8' }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                    <Title style={{marginTop:10,marginLeft:20}}>Select Category</Title>
                    </Body>
                  </Header>
                  <Content>
                    <Card style={{height:35,backgroundColor:'#fff'}}><Text style={{textAlign:'center',marginTop:10,color:'#000'}}>SELECT TYPE</Text></Card>
                    {this.chunkArray(this.state.cardss, 2).map((categories, ind) => <Content style={{ margin: '1%' }} key={ind}>
           <Row>
             {categories.map((item, index) => {
               return (<Col style={styles.col} key={index} >
                 <TouchableOpacity onPress={()=>that.handleClick(index)}>
         
         <View style={{flex:1,flexDirection:'row'}}>
         <Card style={{ width:160,height:50,marginLeft:10,marginRight:10,paddingLeft:10,paddingTop:15, elevation:3, backgroundColor : (item.isSelected ? '#48c7e8': '#fff') }}>
                      <Text style={styles.text} onPress={()=>that.props.navigation.navigate('AddNewProduct')} numberOfLines={3}>{item.name}</Text>
                      </Card>
                      </View>
                      </TouchableOpacity>
               </Col>);
             })}
           </Row>
         </Content>)}

                    </Content>
              </Container>
    );
}
}
const styles = StyleSheet.create({
    text: {
         marginBottom:10,
         marginRight:15
    },
});