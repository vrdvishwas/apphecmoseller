import { TOKEN_VALUE, USER } from '../actions/Type';

const initialState = {
    token: null,
    user: {}
};

const HAReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOKEN_VALUE:
      return {
        ...state,
        token: action.token,
      };
    case USER:
      return {
      ...state,
      user: action.user
      };
    default:
      return state;
  }
};

export default HAReducer;
