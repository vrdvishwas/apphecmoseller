import { TOKEN_VALUE, USER } from './Type';

export const tokenValue = (data) => ({
    type: TOKEN_VALUE,
    token: data,
  }); 

export const userData = (data) => ({
type: USER,
user: data
});  
