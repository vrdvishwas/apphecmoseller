import { createStore, combineReducers } from 'redux';
import HAReducer from './reducers/TokenReducer';

const rootReducer = combineReducers({
  HAReduce: HAReducer
});

const configureStore = () => { 
  return createStore(rootReducer);
};
export default configureStore;
