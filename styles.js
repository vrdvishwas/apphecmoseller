const React = require('react-native');

const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
	header: {
		backgroundColor: '#48c7e8',
		textAlign: 'center',
		justifyContent: 'center'
	},
	SplashScreen_RootView:
	{
		justifyContent: 'center',
		flex:1,
		margin:0,
		position: 'absolute',
		width: '100%',
		height: '100%',
		
	},
 
	SplashScreen_ChildView:
	{
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#48c7e8',
		flex:1,
		margin: 0,
		height: '100%',
		width: '100%'
	},
	home: {
		marginTop: Platform.OS === 'android' ? deviceWidth / 16 : deviceWidth / 15,
		marginLeft: Platform.OS === 'android' ? deviceWidth / 22 : deviceWidth / 21
	},
	hamburger: {
		marginTop: Platform.OS === 'android' ? 5 : 0
	},
	contents: {
		marginTop: 20,
		marginHorizontal: 10
	},
	homeprice: { 
		fontSize: 12
			   },
	homeviews: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center'
	},
	homecards: {
		width: Platform.OS === 'android' ? deviceWidth / 3 : deviceWidth / 3,
		height: Platform.OS === 'android' ? deviceHeight / 3.5 : deviceHeight / 3.5,
		flex: 1,
		alignContent: 'center',
		alignItems: 'center',
		paddingLeft: Platform.OS === 'android' ? deviceWidth / 45 : deviceWidth / 40
	},
	homesvgs: {
		marginVertical: 10
	},
	hometext: {
		fontSize: 12,
		marginVertical: 5
	},
	hometxt: {
		textAlign: 'center',
		width: Platform.OS === 'android' ? deviceWidth / 3 : deviceWidth / 3,
		fontSize: 10,
		paddingLeft: 0,
		paddingRight: Platform.OS === 'android' ? deviceWidth / 20 : deviceWidth / 20
	},
	homeincard: {
		width: Platform.OS === 'android' ? deviceWidth / 10 : deviceWidth / 10,
		height: Platform.OS === 'android' ? deviceHeight / 7 : deviceHeight / 7,
		flex: 1,
		alignContent: 'center',
		alignItems: 'center',
		paddingLeft: 0
	},
	homebuttong: {
		height: 27,
		width: 105,
		marginTop: 10,
		backgroundColor: '#B4DBFF'
	},
	hamburgerIcon: { fontSize: 30, color: '#fff', marginTop: 20 },
	loginimages: {
		marginLeft: deviceWidth / 5
	},
	loginbtn: {
		backgroundColor: '#48c7e8',
		width: deviceWidth / 2.5,
		marginTop: Platform.OS === 'android' ? deviceWidth / 40 : deviceWidth / 4,
		marginLeft: deviceWidth / 3.5, 
		marginBottom: 15
	},
	logintxt: {
		// backgroundColor: '#48c7e8',
		fontSize: 12,
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		textAlign: 'center'
	},
	loginerror: {
		borderBottomWidth: 1.5,
		borderBottomColor: 'red',
		position: 'relative',
		top: 0
	},
	loginforgot: {
		color: '#48c7e8',
		marginBottom: deviceHeight / 40,
		marginLeft: deviceHeight / 5.5
	},
	logindont: {
		color: '#000',
		marginLeft: deviceHeight / 8
	},
	registeropenstore: {
		backgroundColor: '#48c7e8',
		marginTop: 20,
		// marginLeft: 100,
		width: deviceWidth,
		textAlign: 'center',
		alignItems: 'center',
		justifyContent: 'center',
		marginBottom: deviceHeight / 29
	},

	register_already_signup: {
		color: '#48c7e8',
		marginBottom: deviceHeight / 5,
		marginLeft: deviceWidth / 4
	},
	profileicon: { marginTop: Platform.OS === 'android' ? deviceWidth / 40 : deviceWidth / 40 },
	main: {
		flex: 1,
		marginTop: 0,
		paddingLeft: 0,
		paddingRight: 0,
		paddingBottom: 1,
		alignItems: 'stretch'
	},
	MainContainer:
        {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0
        },
    
	toolbarButton: {
		fontSize: 20,
		width: 28,
		height: 28,
		textAlign: 'center'
	},
	italicButton: {
		fontStyle: 'italic'
	},
	boldButton: {
		fontWeight: 'bold'
	},
	underlineButton: {
		textDecorationLine: 'underline'
	},
	lineThroughButton: {
		textDecorationLine: 'line-through'
	},
	uptxt: {
		marginLeft: 10,
		color: "gray" 
	},
	upimg: {
		width:"93.5%" ,
		height: 200,
		marginHorizontal: 10,
		marginVertical: 10
	},
	upubtn: {
		width: Platform.OS === 'android' ? 150 : 149,
		marginLeft: Platform.OS === 'android' ? 10 : 10,
		height: Platform.OS === 'android' ? 33 : 32,
		marginTop: Platform.OS === 'android' ? 10 : 11,
		marginBottom: Platform.OS === 'android' ? 10 : 11
	},
	upbtntxt: { fontSize: 12, textAlign: 'center' },
	nortxt: {
		marginLeft: Platform.OS === 'android' ? deviceWidth / 50 : deviceWidth / 25,
		marginRight: Platform.OS === 'android' ? deviceWidth / 40 : deviceWidth / 25,
		width: Platform.OS === 'android' ? 100 : 101,
		fontSize: 12,
		marginTop: Platform.OS === 'android' ? deviceWidth / 65 : deviceWidth / 60
	},
	text: {
		fontWeight: 'bold',
		marginLeft: Platform.OS === 'android' ? deviceWidth / 50 : deviceWidth / 50,
		width: Platform.OS === 'android' ? 100 : 101,
		fontSize: 13,
		color: 'gray'
	},
	vieworder: {
		color: '#48c7e8',
		marginRight: Platform.OS === 'android' ? deviceWidth / 25 : deviceWidth / 25,
		fontSize: 18,
		marginTop: Platform.OS === 'android' ? deviceWidth / 40 : deviceWidth / 40,
		marginBottom: Platform.OS === 'android' ? deviceWidth / 25 : deviceWidth / 25,
		textAlign: 'right',
		fontWeight: '500'
	},
	carddeta: {
		width: Platform.OS === 'android' ? 325 : 336,
		marginTop: Platform.OS === 'android' ? deviceWidth / 25 : deviceWidth / 25,
		marginLeft: Platform.OS === 'android' ? deviceWidth / 40 : deviceWidth / 40,
		marginRight: Platform.OS === 'android' ? deviceWidth / 100 : deviceWidth / 100,
		paddingLeft: 5
	},
	btn: {
		width: Platform.OS === 'android' ? 100 : 100,
		height: Platform.OS === 'android' ? 25 : 26,
		marginLeft: Platform.OS === 'android' ? 45 : 45,
		marginTop: Platform.OS === 'android' ? 5 : 5,
		marginRight: Platform.OS === 'android' ? 12 : 12
	},
	btntext: {
		fontSize: 10,
		textAlign: 'center'
	},
	datepicker: {width: "99%", marginVertical: 8 },
	submitbtn: {
		width: Platform.OS === 'android' ? 120 : 121,
		height: Platform.OS === 'android' ? 35 : 36,
		marginLeft: Platform.OS === 'android' ? 130 : 131,
		marginTop: Platform.OS === 'android' ? 10 : 11
	},
	orderlgtxt: {
		marginTop: Platform.OS === 'android' ? 10 : 11,
		marginLeft: Platform.OS === 'android' ? 10 : 11,
		marginRight: Platform.OS === 'android' ? 5 : 6,
		fontWeight: '300',
		fontSize: 14
	},
	ordertxt: {
		marginTop: Platform.OS === 'android' ? 10 : 10,
		marginLeft: Platform.OS === 'android' ? 10 : 10,
		fontWeight: '400',
		fontSize: 14
	},
	cardorder: {
		marginLeft: 5,
		marginRight: 5,
		paddingVertical: 20
	},
	orderText: {
		marginLeft: Platform.OS === 'android' ? 11 : 11
	},
	heading: {
		color: '#48c7e8',
		fontWeight: 'bold',
		fontSize: 20,
		marginLeft: Platform.OS === 'android' ? 10 : 10
	},
	font: { fontWeight: 'bold' },
	listviews: { flex: 1, flexDirection: 'row' },
	productimg: {
		width: Platform.OS === 'android' ? 100 : 100,
		height: Platform.OS === 'android' ? 100 : 100,
		marginTop: Platform.OS === 'android' ? deviceWidth / 40 : deviceWidth / 60,
		marginLeft: Platform.OS === 'android' ? deviceWidth / 40 : deviceWidth / 40,
		marginRight: Platform.OS === 'android' ? deviceWidth / 40 : deviceWidth / 40
	},
	viewcolumn: { flex: 1, flexDirection: 'column' },
	producttitle: {
		marginTop: Platform.OS === 'android' ? deviceWidth / 40 : deviceWidth / 40,
		fontWeight: 'bold'
	},
	productheading: { marginTop: Platform.OS === 'android' ? deviceWidth / 40 : deviceWidth / 40 },
	priceview: {
		flex: 1,
		flexDirection: 'row',
		paddingLeft: Platform.OS === 'android' ? deviceWidth / 4 : deviceWidth / 4,
		paddingBottom: Platform.OS === 'android' ? deviceWidth / 30 : deviceWidth / 30
	},
	price: {
		marginLeft: Platform.OS === 'android' ? deviceWidth / 10 : deviceWidth / 10,
		marginRight: Platform.OS === 'android' ? deviceWidth / 10 : deviceWidth / 10
	},
	productunits: {
		color: 'orange',
		marginLeft: Platform.OS === 'android' ? deviceWidth / 10 : deviceWidth / 10
	},
	shippingcard: {
		backgroundColor: '#48c7e8',
		paddingLeft: Platform.OS === 'android' ? deviceWidth / 40 : deviceWidth / 40,
		marginTop: Platform.OS === 'android' ? deviceWidth / 40 : deviceWidth / 40,
		marginBottom: Platform.OS === 'android' ? deviceWidth / 40 : deviceWidth / 40
	},
	shippingbtn: {
		width: Platform.OS === 'android' ? deviceWidth / 3 : deviceWidth / 4,
		height: Platform.OS === 'android' ? deviceWidth / 10 : deviceWidth / 13,
		marginLeft: Platform.OS === 'android' ? deviceWidth / 4 : deviceWidth / 3,
		marginTop: Platform.OS === 'android' ? deviceWidth / 30 : deviceWidth / 30,
		marginBottom: Platform.OS === 'android' ? deviceWidth / 30 : deviceWidth / 30,
		backgroundColor: "#48c7e8"
	},
	savetxt: {
		textAlign: 'center',
		marginLeft: Platform.OS === 'android' ? deviceWidth / 13 : deviceWidth / 30
  },
  edittxt: {
		textAlign: 'center',
		marginLeft: Platform.OS === 'android' ? deviceWidth / 23 : deviceWidth / 30
	},

	edit: {
		marginTop: Platform.OS === 'android' ? deviceWidth / 10 : deviceWidth / 30,
		marginBottom: Platform.OS === 'android' ? deviceWidth / 30 : deviceWidth / 30,
		position: 'relative',
		left: Platform.OS === 'android' ? deviceWidth / 2 : deviceWidth / 4,
		marginLeft: Platform.OS === 'android' ? deviceWidth / 5 : deviceWidth / 4,
		width: Platform.OS === 'android' ? deviceWidth / 4 : deviceWidth / 2,
		height: Platform.OS === 'android' ? deviceWidth / 10 : deviceWidth / 13
	},
	canceled:{
	color:"red"
	},
	closed:{
		color:"red"
	},
	complete:{
		color: "green"
	},
	holded:{
		color:"#EF820D"
	},
	pending:{
		color:"#FF9333"
	},
	pending_payment:{
		color: "#FF9333"
	},
	processing:{
		color:"#33BAFF"
	},
	Upcoming:{
		color:"green"
	},
	Dispatch_requested:{
		color: "#33BAFF"
	},
	Ready_for_pickup:{
		color: "#02144D"
	},
	modalContent: {
		backgroundColor: '#ffffff', 
		marginTop: 0,
		bottom: 0,
		borderRadius: 4,
		borderColor: 'rgba(0, 0, 0, 0.1)',		  
		},
		bottomModal: {	
		backgroundColor: '#ffffff',
			margin: 0,
			bottom: 0
		},
		filterFields:{ 
			marginHorizontal: 10,
			marginVertical: 5
	},
	lockIconPopUp:{
		margin: 200,
		backgroundColor: "#f4f4f4"
	},
	modalPopUp: {
		alignItems: 'center',
		backgroundColor: '#48c7e8', 
		width: 250,
		height: 250,
		marginTop: 100,
		marginLeft: 50, 
		borderRadius: 20, 
		padding: 20  
	 },	
};
