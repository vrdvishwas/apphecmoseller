import axios from 'axios';

export class DataCall {


	static async products(count, total, token, from_date, to_date, status, search) {
		
		const responseHusky = await axios.post(`https://www.hecmo.com/rest/V1/seller/products?page=${count}&per_page=${total}&from_date=${from_date}&to_date=${to_date}&status=${status}&search=${search}`, {},{
			headers: {
				Authorization: `Bearer ${token}`
			}
		});
		const responseJsonHusky = await responseHusky.data;
		return responseJsonHusky;
	}

	static async categories(filter, code) {
		filter += (filter ? '&' : '?') + 'code=' + code;
		const categoryRequest = await axios.get(`https://www.hecmo.com/rest/V1/agent/categories${filter}`);
		const categoryResponse = await categoryRequest.data;
		return categoryResponse;
	}
	static async forgotPassword(email) {
		const forgotPasswordRequest = await axios.post('https://www.hecmo.com/rest/V1/seller/request/password', {
			email
		});
		return await forgotPasswordRequest.status;
	}
	static async filterData(code) {
		const filterDataRequest = await axios.get(`https://www.hecmo.com/rest/V1/agent/attributes?code=${code}`);
		const filterDataResponse = await filterDataRequest.data;
		return filterDataResponse;
	}

	static async homeData(token) {
        const homeDataRequest = await axios.post(`https://www.hecmo.com/rest/V1/seller/dashboard`, {}, {
        headers: {
        Authorization: `Bearer ${token}`
		}
        });
		const homeDataResponse = await homeDataRequest.data;
        return homeDataResponse;
    }	
	static async profileData(token) {
        const profileDataRequest = await axios.post(`https://www.hecmo.com/rest/V1/seller/profile`, {}, {
        headers: {
        Authorization: `Bearer ${token}`
		}
        });
		const profileDataResponse = await profileDataRequest.data;
        return profileDataResponse;
	}
	
	static async chartApiData(ty, token) {
        const chartDataRequest = await axios.post(`https://www.hecmo.com/rest/V1/seller/chart?duration=${ty}`, {}, {
        headers: {
        Authorization: `Bearer ${token}`
		}
        });
		const chartDataResponse = await chartDataRequest.data;
        return chartDataResponse;
    }

	static async updateProfile(postData, token) {
	// console.log('testing in update', postData);
        const changePasswordRequest = await axios.post('https://www.hecmo.com/rest/V1/seller/updateprofile', postData, {
        headers: {
			Authorization: `Bearer ${token}`,
			'Content-Type' : 'multipart/form-data'
        }
		});
		const changePasswordResponse = await changePasswordRequest.data;
		// console.log("ssssssdsdsdsdllllllllllllllllllllmmmm",changePasswordResponse);
        return changePasswordResponse;
    }

	static async register(postData, token) {
        const registerRequest = await axios.post('https://www.hecmo.com/rest/V1/seller/register', postData, {
        headers: {
			Authorization: `Bearer ${token}`,
			'Content-Type' : 'multipart/form-data'
        }
        });
		const registerResponse = await registerRequest.data;
        return registerResponse;
	}
	
	static async orderHistoryData(count, total, token, from_date, to_date, status, search, cancel_by) {
		console.log(`https://www.hecmo.com/rest/V1/seller/orders?page=${count}&from_date=${from_date}&to_date=${to_date}&status=${status}&search=${search}&cancel_by=${cancel_by}`);
		const responseHistory = await axios.post(`https://www.hecmo.com/rest/V1/seller/orders?page=${count}&from_date=${from_date}&to_date=${to_date}&status=${status}&search=${search}&cancel_by=${cancel_by}`, {}, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		});
		const responseJsonHistory = await responseHistory.data;
		console.log(responseJsonHistory);
		return responseJsonHistory;
	}

	static async orderDetailsData(id, token) {
		const requestOrder = await axios.post(`https://www.hecmo.com/rest/V1/seller/order/detail?order_id=${id}`, {}, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		});
		const responseOrder = await requestOrder.data;
		console.log(responseOrder);
		return responseOrder;
	}

	static async TransactionList(token) {
		const requestTransaction = await axios.post(`https://www.hecmo.com/rest/V1/seller/transactions`, {}, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		});
		const responseTransaction = await requestTransaction.data;
		return responseTransaction;
	}
	static async ProductDetail(id, token) {
		const requestProductDetail = await axios.post(`https://www.hecmo.com/rest/V1/seller/product?product_id=${id}`, {}, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		});
		const responseProductDetail = await requestProductDetail.data;
		console.log("product", requestProductDetail.data);
		return responseProductDetail;
	}

	static async OnesignalUpdateCustomUserID(oldUserID,hashedValue) {
        const OnesignalUpdateCustomUserIDRequest = await axios.put(`https://onesignal.com/api/v1/players/${oldUserID}`,{
            app_id: '5123111b-d184-4730-91e0-ba23745ef0b6',
            external_user_id: hashedValue
        });
        const OnesignalUpdateCustomUserIDResponse = await OnesignalUpdateCustomUserIDRequest.status;
        return OnesignalUpdateCustomUserIDResponse;
	}


	static async readyToDispatchButtonAction(Orderid,Usertoken) {
		const  readyToDispatchButtonActionRequest= await axios.get(`https://www.hecmo.com/rest/V1/seller/order/dispatch?id=${Orderid}`,{
			headers: {
				Authorization: `Bearer ${Usertoken}`
			}
		});
		const readyToDispatchButtonActionResponse = await readyToDispatchButtonActionRequest.status;
		console.log(readyToDispatchButtonActionResponse);
		return readyToDispatchButtonActionResponse;
	}

	static async rmaData(count, token) {
	// ?page=${count}&per_page=${total}
		const RmaDataRequest= await axios.post(`https://www.hecmo.com/rest/V1/seller/allrma?page=${count}`, {}, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		});

		const RmaDataResponse= await RmaDataRequest.data;
		return RmaDataResponse;
	}
}
