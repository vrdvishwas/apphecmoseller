import React from 'react';
import { Root } from 'native-base';
import 
{  
  createSwitchNavigator, 
  createAppContainer } 
from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';
import Home from './components/Home';
import Login from './components/Login';
import Register from './components/Register';
import DrawerItems from './components/DrawerItems';
import Profile from './components/Profile';
import OrderDetails from './components/OrderDetails';
import OderHistory from './components/OderHistory';
import ProductUpload from './components/ProductUpload';
import Transaction from './components/Transaction';
import MyProductList from './components/MyProductList';
import Shipping from './components/Shipping';
import MyDetails from './components/MyDetails';
import ReturnsCancel from './components/ReturnsCancel';
import ReturnView from './components/ReturnView';
import AuthCheck from './components/AuthCheck';
import ForgotPassword from './components/ForgotPassword';
import ChangePassword from './components/ChangePassword';
import ProductDetail from './components/ProductDetail';
import ImageZoomPage from'./components/ImageZoomPage';
import RMAPage from './components/RMAPage';

const Drawer = createDrawerNavigator(
  {
    Home: { screen: Home }
}, 
  {
    initialRouteName: 'Home',
    contentOptions: {
      activeTintColor: '#e91e63'
    },
    contentComponent: props => <DrawerItems {...props} />
  }
);

const AfterLogin = createStackNavigator(
  {
    Drawer: { screen: Drawer },
    Home: { screen: Home },
    Profile: { screen: Profile },
    ProductDetail: { screen: ProductDetail },
    OrderDetails: { screen: OrderDetails },
    ChangePassword: { screen: ChangePassword },
    OderHistory: { screen: OderHistory },
    ProductUpload: { screen: ProductUpload },
    Transaction: { screen: Transaction },
    MyProductList: { screen: MyProductList },
    Shipping: {screen: Shipping},
    ReturnView: {screen: ReturnView},
    MyDetails: {screen: MyDetails},
    ReturnsCancel: {screen: ReturnsCancel},
    Login: {screen: Login},
    ImageZoomPage: {screen: ImageZoomPage },
    RMAPage: {screen: RMAPage }
  },
  {
    initialRouteName: 'Drawer',
    headerMode: 'none'
  }
);

const BeforeLogin = createStackNavigator({
	Login: { screen: Login,
    navigationOptions: {
      header: null 
    } },
	Register: { screen: Register,
    navigationOptions: {
     header: null 
    } },
    ForgotPassword: { screen: ForgotPassword,
      navigationOptions: {
       header: null
      } }
});

const AppSwitchNavigator = createSwitchNavigator({
  AuthCheck: { screen: AuthCheck },
  BeforeLogin: { screen: BeforeLogin },
  AfterLogin: { screen: AfterLogin },

});

const AppContainer = createAppContainer(AppSwitchNavigator);

export default () =>
  <Root>
    < AppContainer />
  </Root>;
